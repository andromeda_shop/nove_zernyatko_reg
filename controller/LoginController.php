<?php

/**
 * Created by PhpStorm.
 * User: i.yuzkiv
 * Date: 04.02.2019
 * Time: 15:02
 */
class LoginController extends \core\BaseController
{

    public function action_main () {

        $error = null;
        if (!empty($_POST) AND isset($_POST['error_id']))
        {
            $error = $this->config->get($_POST['error_id']);
        }

        $this->View
            ->setTitle("Авторизація ")
            ->setActiveMenu("login")
            ->render("login/form", [
                'captcha_public_key' => $this->config->get("recaptcha_v3_publick_key"),
                'error_login' => $error
            ]);

    }

    public function action_lk() {
        
        $mng_login = new \module\Login();
        if (!$mng_login->checkAuthorized()) $this->redirectToUrl("/login/");
        $session_info = $_SESSION['lk_user_info'];

        $regions = $this->db->CreateQuery("SELECT DISTINCT `oblast` FROM `dov_addrs_ukarin1`");

        $userInfo = $mng_login->getUserInfo();

        $this->View
            ->setTitle("Особистий кабінет")
            ->setActiveMenu("login")
            ->setUserScripts([
                "/view/_system/js/jquery.maskedinput.js"
            ])
            ->render("login/lk", [
                'session_info' => $session_info,
                'info' => $userInfo,
                'param_pids_diy_dyr' => explode(";", $this->config->get("param_pids_diy_dyr")),
                'regions' => $regions,
            ]);
    }

    /**
     * Download file
     */
    public function action_download_file () {

        if (isset($_POST['url']))
        {
            if (file_exists($_POST['url'])) {

                $base_name = basename($_POST['url']);
                $file_name = ($_POST['file_name'] != "") ? $_POST['file_name'].substr($base_name, strpos($base_name,'.',0)) : $base_name;

                /*header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="'.$file_name.'"');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($_POST['url']));
                readfile($_POST['url']);*/

                header('Pragma: public');
                header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the Past
                header('Expires: 0');
                header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                header('Cache-Control: private', false);
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="'.$file_name.'"');
                readfile($_POST['url']);

                exit;
            }
        }
    }

    /**
     *
     */
    public function action_valid_login () {

        if(isset($_POST['recaptcha_response']) && !empty($_POST['recaptcha_response'])) {
            //your site secret key
            $secret = $this->config->get("recaptcha_v3_private_key");
            //get verify response data
            $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['recaptcha_response']);
            $responseData = json_decode($verifyResponse);

           if ($responseData->success) {

                 $mng_login = new \module\Login();

                $valid_login = $mng_login->validLogin($_POST);
                if (is_bool($valid_login) and $valid_login == true)
                {
                    echo json_encode([true]); exit();
                }elseif (is_string($valid_login))
                {
                    echo json_encode([false, $valid_login]); exit();
                }

            } else {
                echo json_encode(['recaptcha_false']); exit;
            }
        } else {
            echo json_encode(['recaptcha_false']); exit;
        }

    }

    public function action_updateInfo() {
        $mng_login = new \module\Login();
        if (!$mng_login->checkAuthorized() OR empty($_POST)) $this->redirectToUrl("/login/");

        $session_info = $_SESSION['lk_user_info'];
        $nz_admin_update  = new \nz_admin\model\UpdateRegData();


        if ($_POST['guest_info_remove_items'] != "") {
            $_POST['guest_info_remove_items'] = explode(",", substr($_POST['guest_info_remove_items'], 0, -1));
        }

        switch ($session_info['type'])
        {
            case 0:
                $post_data = [
                    'base_info' => $_POST['base_info'],
                    'guset_info' => $_POST['guest_info'],
                    'dogovyr_info' => $_POST['dogovyr_info'],
                    'remove_guest_id' => $_POST['guest_info_remove_items']
                ];

                $nz_admin_update->saveBackOriginalData($session_info['id_partner_main'], "partner");
                $result = $nz_admin_update->updatePartnerData($session_info['id_partner_main'], $post_data);
                break;
            case 1:
                $post_data = [
                    'base_info' => $_POST['base_info'],
                    'guset_info' => (!empty($_POST['guest_info'])) ? $_POST['guest_info'] : array(),
                    'remove_guest_id' => $_POST['guest_info_remove_items']
                ];
                $nz_admin_update->saveBackOriginalData($session_info['id_guest_main'], "guests");
                $result = $nz_admin_update->updateGuestsData($session_info['id_guest_main'], $post_data);
                break;
        }



        echo $result;
        exit();

    }




}