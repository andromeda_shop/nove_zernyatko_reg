<?php

/**
 * Created by PhpStorm.
 * User: i.yuzkiv
 * Date: 31.01.2019
 * Time: 15:58
 */
class Registration_agentController extends \core\BaseController
{
    /**
     * Крок 0 - Вибір реєстрації
     */
    public function action_index() {
        $this->View
            ->setTitle("Реєстрація")
            ->setH1Title("Реєстрація на &#8547; міжнародний Форум Аграрних Інновацій \"Нове Зернятко 2019\"")
            ->setActiveMenu("registration")
            ->render( "registration/index", [] );
    }

    /**
     *  /registration/step-one
     *  Вибір тарифного плану
     */
    public function action_regPartner_stepOne () {

        $package_services_data = $this->db
            ->setSelect("*")
            ->setFrom('package_services')
            ->setOrderBy("`postion` ASC")
            ->all();

        foreach ($package_services_data as $key => $item)
        {
            if ($item['other'] != NULL)
            {
                $package_services_data[$key]['other'] = explode(";", $item['other']);
            }

        }
        
        $this->View
            ->setTitle("Реєстрація учсників")
            ->setH1Title("Реєстрація учасника на Ⅳ міжнародний Форум Аграрних Інновацій \"Нове Зернятко 2019\"")
            ->setActiveMenu("registration")
            ->render( "registration/reg_agent_step_one", [
                'package_services_data' => $package_services_data,
                'hashField' => $this->hashField
            ] );
    }

    /**
     * /registration/step-two
     * Реквізити для договору
     */
    public function action_regPartner_stepTwo () {

        if (empty($_POST) || !isset($_POST['package_services']))  $this->redirectToUrl("/registration/step-one");

        /*Перевірка HTML Хеш поля*/
        if (isset($_POST["hashField"]))
        {
            if (!$this->validHashField($_POST["hashField"]))
            {
                $this->redirect404();
            }
            else {
                unset($_POST['hashField']);
            }

        }else $this->redirect404();

        

        $_SESSION['registration_info']['select_package_services'] = $_POST['package_services'];

        $regions = $this->db->CreateQuery("SELECT DISTINCT `oblast` FROM `dov_addrs_ukarin1`");

        $this->View
            ->setTitle("Реєстрація учсників")
            ->setH1Title("Реєстрація учасника форуму")
            ->setActiveMenu("registration")
            ->setUserScripts([
                "/view/_system/js/jquery.maskedinput.js"
            ])
            ->render( "registration/reg_agent_step_two", [
                'regions' => $regions,
                'captcha_public_key' => $this->config->get("recaptcha_v3_publick_key"),
                'hashField' => $this->hashField,
                'param_pids_diy_dyr' => explode(";", $this->config->get("param_pids_diy_dyr")),
            ] );
    }

    public function action_regPartner_stepThree () {

        /*$_SESSION['registration_info']['select_package_services']*/
        if (!isset($_SESSION['registration_info']['select_package_services']) or $_SESSION['registration_info']['select_package_services'] == 0)
        {
            $this->redirectToUrl("/registration/step-one");
        }
        $diyalnist = $this->db -> setSelect("*") -> setFrom("diyalnist") -> setOrderBy("`name` ASC") -> setOrderBy("id ASC") -> all();
        foreach ($diyalnist as $key=> $item_d){
            $diyalnist[$key]['item'] = $this->db->setSelect("*")->setFrom("diyalnist_item")->setWhere("`id_main`=".$item_d['id']) -> all();
        }

        $package_services_info = $this->db
            ->setSelect("*")
            ->setFrom("package_services")
            ->setWhere("id=".$_SESSION['registration_info']['select_package_services'])
            ->one();

        $this->View
            ->setTitle("Реєстрація учсників")
            ->setH1Title("Реєстрація учасника Форуму")
            ->setActiveMenu("registration")
            ->setUserScripts([
                "/view/_system/js/jquery.maskedinput.js"
            ])
            ->render("registration/reg_agent_step_three", [
                'diyalnist' => $diyalnist,
                'package_services_info' => $package_services_info,
                'hashField' => $this->hashField
            ]);
    }
    public function action_regPartner_stepFour () {

        if (!isset($_SESSION['id_partner']))
        {
            echo '<script type="application/javascript"> location.href="/404" </script>';
        }
        $mng_reg_agent = new \module\RegistrationPartner();
        $data = $mng_reg_agent->getDataForStepFour($_SESSION['id_partner']);

        //send mail
        (new \module\RegistrationPartner()) -> sendMail(1, $data['info_main']['email_v'], $_SESSION['id_partner']);


        $this->View
            ->setTitle("Реєстрація учсників")
            ->setH1Title("Реєстрація учасника Форуму")
            ->render("registration/reg_agent_step_four", [
                "guests" => $data['info_guests'],
                "main" => $data['info_main'],
                "dogovyr" => $data['info_dogovyr'][0],
                "cnt_guests" => count($data['info_guests']),
                "diyalnist" => $data['diyalnist'],
                "package_services" => $data['package_services']

            ]);

    }
    /**
     * Перевірка реквізитів для договору на дублікати
     */
    public function action_validation_data_1 () {

        if (empty($_POST) || !isset($_POST['recaptcha_response']))
        {
            echo '<script type="application/javascript"> location.href="/404" </script>';
        }

        /*Перевірка HTML Хеш поля*/
        if (isset($_POST["hashField"]))
        {
            if (!$this->validHashField($_POST["hashField"]))
            {
                echo json_encode(array("NO_ACCESS")); exit;
            }
            else {
                unset($_POST['hashField']);
            }

        }else
        {
            echo json_encode(array("NO_ACCESS")); exit;
        }

            $mng_reg_agent = new \module\RegistrationPartner();
            echo json_encode(array($mng_reg_agent->validateRegDataStep_1($_POST)));

    }

    /**
     * Валідація даних про відповідального за реєстрацію, гостей від компанії, файл логотип
     */
    public function action_validation_data_2 () {

        if (empty($_POST) or empty($_FILES) or empty($_SESSION['registration_info']))
        {
            echo '<script type="application/javascript"> location.href="/404" </script>';
            //exit;
        }

        /*Перевірка HTML Хеш поля*/
        if (isset($_POST["hashField"]))
        {
            if (!$this->validHashField($_POST["hashField"]))
            {
                echo json_encode(array("NO_ACCESS")); exit;
            }
            else {
                unset($_POST['hashField']);
            }
        }else
        {
            echo json_encode(array("NO_ACCESS")); exit;
        }

        //Перевірка файлу
        $img_format = array(".pdf", ".eps", ".cdr", ".ai",'.zip','.rar');
        if(!in_array(substr($_FILES["logo-file"]["name"],-4),$img_format) OR $_FILES["logo-file"]['size'] > 10000000)
        {
            echo json_encode( array('Формат вашо файлу не підтримуюється, або розмір файлу перевищує 10Мб. Підтримуються файли тільки формату: '.implode($img_format,' , ')) ); exit;

        }

        $mng_reg_agent = new \module\RegistrationPartner();
        //Перевірка даних форми
        $validate = $mng_reg_agent->validateRegDataStep_2($_POST);
        if ($validate === true) {
            
            if ($mng_reg_agent->savePartner() AND $mng_reg_agent->saveLogo($_FILES))
            {
                echo json_encode(array("true")); exit;
            }
            else
            {
                echo json_encode(array('Виникла внутрішня помилка, спробуйте зареєструватись пізніше. :( ')); exit;
            }

        }else {
            echo json_encode($validate);
        }



    }


    /**
     * @param string $part
     * Отримати адреси в списки
     */
    public function action_getAddressValue( $part  ) {
        if (!isset($_SESSION['lk_user_info']))
        {
            if (empty($_POST) or !isset($_SESSION['registration_info'])) echo '<script type="application/javascript"> location.href="/404" </script>';
        }

        $result = array();
        switch ($part)
        {
            case 1:
                $result = $this->db->CreateQuery("SELECT DISTINCT `rayon` FROM `dov_addrs_ukarin1` WHERE `oblast` LIKE '%".$_POST['region']."%' AND `rayon` NOT LIKE '' ORDER BY `dov_addrs_ukarin1`.`rayon`  ASC;");
                break;
        }
        echo json_encode($result); exit;
    }

}