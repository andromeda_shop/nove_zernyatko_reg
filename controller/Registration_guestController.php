<?php

/**
 * Created by PhpStorm.
 * User: i.yuzkiv
 * Date: 08.02.2019
 * Time: 16:32
 */
class Registration_guestController extends  \core\BaseController
{

    public function action_reg_guest_main () {

        $diyalnist = $this->db -> setSelect("*") -> setFrom("diyalnist") -> setOrderBy("`name` ASC") -> setOrderBy("id ASC") -> all();
        foreach ($diyalnist as $key=> $item_d){
            $diyalnist[$key]['item'] = $this->db->setSelect("*")->setFrom("diyalnist_item")->setWhere("`id_main`=".$item_d['id']) -> all();
        }
        
        $this->View
            ->setTitle("Реєстрація відвідувачів ")
            ->setH1Title("Реєстрація відвідувачів на &#8547; міжнародний Форум Аграрних Інновацій \"Нове Зернятко 2019\"")
            ->setActiveMenu("registration")
            ->setUserScripts([
                "/view/_system/js/jquery.maskedinput.js"
            ])
            ->render( "registration_guests/main", [
                'captcha_public_key' => $this->config->get("recaptcha_v3_publick_key"),
                'diyalnist' => $diyalnist,
                'hashField' => $this->hashField
            ] );
            
    }

    public function action_validationData () {

       if (isset($_POST) and !empty($_POST))
       {
           /*// Build POST request:
           $recaptcha_url = 'https://www.google.com/recaptcha/api/siteverify';
           $recaptcha_secret = $this->config->get("recaptcha_v3_private_key");
           $recaptcha_response = $_POST['recaptcha_response'];
           // Make and decode POST request:
           $recaptcha = file_get_contents($recaptcha_url . '?secret=' . $recaptcha_secret . '&response=' . $recaptcha_response);
           $recaptcha = json_decode($recaptcha);*/

           /*Перевірка HTML Хеш поля*/
           if (isset($_POST["hashField"]))
           {
               if (!$this->validHashField($_POST["hashField"]))
               {
                   echo json_encode(array("NO_ACCESS")); exit;
               }
               else {
                   unset($_POST['hashField']);
               }

           }else
           {
               echo json_encode(array("NO_ACCESS")); exit;
           }
           $mng_reg_agent = new \module\RegistrationPartner();

            unset($_POST['recaptcha_response']);

            $validate = $mng_reg_agent->validateRegDataStep_2($_POST, true);

           //$recaptcha->score >= 0.5
           //dump($recaptcha->{"error-codes"});
            if ($validate === true)
            {
                $mng_reg_agent->saveVisitors();
                echo json_encode(array("true"));
            }else echo json_encode($validate);


       }else {
           $this->redirectToUrl("/registration/visitors");
       }
    }

        

    public function action_resultRegistration () {


        if (!isset($_SESSION['id_visitors']) or $_SESSION['id_visitors'] == 0)
        {
            $this->redirectToUrl("/registration/visitors");
        }

        $data = array();
        $data['info_main'] = $this->db
            ->setSelect("*")
            ->setFrom("guests_main")
            ->setWhere("id=".$_SESSION['id_visitors'])
            ->one();
        $info_guests = $this->db
            ->setSelect("*")
            ->setFrom("guests_item")
            ->setWhere("`id_guest_main`= ".$_SESSION['id_visitors'])
            ->all(); 

        $vidpovid = array(
            'pip_guest' => $data['info_main']['pip_v'],
            'mob_num_guest' => $data['info_main']['mob_v'],
            'email_guest' => $data['info_main']['email_v'],
            'posada_guest' => $data['info_main']['posada_v'],
        );
        //Перевіряю на той випадок якщо гостей немає, тобто $info_guests пустий

        if (empty($info_guests) or !isset($info_guests[0]))
        {
            $info_guests[0] = $vidpovid;
        }else {
            array_unshift($info_guests, $vidpovid);
        }

        $data['info_guests'] = $info_guests;
        $data['diyalnist'] = "";
        foreach (explode(",",$data['info_main']['diyalnist_id']) as $item){
            $data['diyalnist'] .= ", ".$this->db->setSelect("name")->setFrom("diyalnist_item")->setWhere("id=".$item)->one()['name'];
        }
        $data['diyalnist'] = substr($data['diyalnist'], 2);

        //send mail
        (new \module\RegistrationPartner()) -> sendMail(2, $data['info_main']['email_v'], $_SESSION['id_visitors']);

        $this->View
            ->setTitle("Реєстрація відвідувачів ")
            ->setH1Title("Дякуємо за реєстрацію")
            ->setActiveMenu("registration")
            ->render("registration_guests/result", [
                "guests" => $data['info_guests'],
                "main" => $data['info_main'],
                "cnt_guests" => count($data['info_guests']),
                "diyalnist" => $data['diyalnist'],
            ]);

    }
    
}