<?php

/**
 * Created by PhpStorm.
 * User: i.yuzkiv
 * Date: 10.01.2019
 * Time: 15:55
 */
class SiteController extends  \core\BaseController
{
    public function action_error_404 () {

        $this->View
            ->setTitle("Такої сторінки не існує ")
            ->render("errors/404", []);
    }
    public function action_error_403 () {
        echo "<h1>403</h1>";
    }
    
    public function action_faq () {

        $this->View
            ->setTitle("Питання відповідь ")
            ->setActiveMenu("faq")
            ->render("faq/main", []);
    }

}