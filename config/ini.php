<?php
/**
 * Created by PhpStorm.
 * User: i.yuzkiv
 * Date: 18.10.2018
 * Time: 11:37
 */

require_once($_SERVER['DOCUMENT_ROOT']."/vendor/autoload.php");

define("_DEBUG", true); // show/hide error
define("_ROUTING_DEBUG", true); // update rout file -(routing.xml.dat)

define("_VIEW_", "view/");
define("_CONFIG_PATCH", "config/");
define("_CONTROLLER_PATCH", "controller/");


define("_VIEW_ADMIN", "nz_admin/view/");
define("_CONFIG_PATCH_ADMIN", "nz_admin/config/");
define("_CONTROLLER_PATCH_ADMIN", "nz_admin/controller/");

$Register= \core\Registry::getInstance();
$Register->set("_DB_", $db = new \core\Database());
$Register->set("_CONFIG_", new \core\config());
$Register->set("Smarty", new Smarty());
$Register->set("BaseModel", new \core\BaseModel());
$Register->set("BaseView", new \core\BaseView());


