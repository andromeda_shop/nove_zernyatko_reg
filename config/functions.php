
<?php

function dump($data, $hr = false) {
        if(is_array($data)) { //If the given variable is an array, print using the print_r function.
                print "<pre>-----------------------\n";
                print_r($data);
                print "-----------------------</pre>";
        } elseif (is_object($data)) {
                print "<pre>==========================\n";
                var_dump($data);
                print "===========================</pre>";
        } else {
                print "=========&gt; ";
                var_dump($data);
                print " &lt;=========";
        }

        if($hr) echo "<hr>";
}


function writeLog ($file_name = "", $str = "") {
        $_LOG_DIR = "log/";
        file_put_contents($_LOG_DIR.$file_name, "\n".date("d.m.y h:s:s")." - ".$str, FILE_APPEND);
}

function truncate($text, $chars = 7000) {

        //specify number fo characters to shorten by
        $text = $text." ";
        $text = substr($text,0,$chars);
        $text = substr($text,0,strrpos($text,' '));
        $text = $text;
        return $text;

}

/*Дерево*/

function form_tree($mess, $parent_id_name)
{
        if (!is_array($mess)) {
                return false;
        }
        $tree = array();
        foreach ($mess as $value) {
                $tree[$value[$parent_id_name]][] = $value;
        }
        return $tree;
}

function build_tree($cats, $parent_id, $lbl_index_name = "label")
{
        if (is_array($cats) && isset($cats[$parent_id])) {
                $tree = '';
                foreach ($cats[$parent_id] as $cat) {
                        $tree .= '<li> <a href="'.$cat['href'].'" >' . $cat[$lbl_index_name];
                        $tree .= build_tree($cats, $cat['id']);
                        $tree .= '</a> </li>';
                }
                $tree .= '';
        } else {
                return false;
        }
        return $tree;
}

/* /Дерево */

function array_orderby()
{
        $args = func_get_args();
        $data = array_shift($args);
        foreach ($args as $n => $field) {
                if (is_string($field)) {
                        $tmp = array();
                        foreach ($data as $key => $row)
                                $tmp[$key] = $row[$field];
                        $args[$n] = $tmp;
                }
        }
        $args[] = &$data;
        call_user_func_array('array_multisort', $args);
        return array_pop($args);
}

/*
 *
 */
function generateToken($onlyNumber = true, $sha1 = true, $cnt = 20) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $token = "";
    if ($onlyNumber)
    {
        for ($i=0; $i<$cnt; $i++)
        {
            $token .= rand(0, 9);
        }

    }else {
        $charsetLen = strlen($characters);
        for ($i = 0; $i < $cnt; $i++) {
            $token .= $characters[rand(0, $charsetLen - 1)];
        }
    }

    if ($sha1) $token = sha1($token);

    return $token;
}