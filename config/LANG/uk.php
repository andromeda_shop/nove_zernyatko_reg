<?php
/**
 * Created by PhpStorm.
 * User: i.yuzkiv
 * Date: 14.02.2019
 * Time: 9:02
 */

return array (
    'pip' => 'ПІП',
    'label_email' => 'Адреса для електронного листування',
    'label_diyalnist' => 'Види діяльності компанії, підприємства',

    'text_privacy' => 'Даю згоду на обробку персональних даних',

    'btn_accept' => "Підтвердити",

    'placeholder_pip' => 'Іванов Іван Іванович',
    'placeholder_mob' => '380 (98) 909 09 09',
    'placeholder_email' => 'i.ivanov@gmail.com',
    'placeholder_posada' => 'Менеджер із збуту',
    'placeholder_name_factory' => 'ПАП Агропродсервіс',


    'start' => [
        'btn_reg_partner' => 'РЕЄСТРАЦІЯ ДЛЯ УЧАСНИКІВ ФОРУМУ',
        'btn_reg_guests' => 'РЕЄСТРАЦІЯ ДЛЯ ВІДВІДУВАЧІВ ФОРУМУ'
    ],
    
    'reg_guest' => [
        'panel_title_vid' => 'Уповноважена особа',
        'panel_title_guest' => 'Представники компанії, підприємства',

        'label_pip_vid' => 'ПІП уповноваженого',
        'label_name_factory' => 'Назва компанії, підприємства',
        'label_email' => 'Адреса для електронного листування',
        'label_posada' => 'Посада уповноваженого',
        'label_mob_num' => 'Номер телефону',

        'btn_add_guest_caption' => 'Додати представника',

        'label_pip_guest' => 'ПІП представника компанії, підприємства',
        'label_posada_guest' => 'Посада представника компанії, підприємства',

        "result_info_factory" => 'Інфомраціїя про компанію, підприємства',
        "result_info_guests" => 'Перелік представників від компанії, підприємства',

        "result_info_panel_title" => 'Інфомрація',
        "result_label_count_guest" => 'Загальна кількість представників',
        "hto_taki_pred" => "Хто такі представники компанії, підприємства ?",
    ],
    'reg_partner' => [
        'step_1_panel_label' => 'Що таке виставкова зона?',
        'step_1_tab_f_name' => 'Назва',
        'step_1_tab_f_price' => 'Вартість',
        'step_1_tab_f_size' => 'Розмір виставкової зони',
        'step_1_tab_f_count_guest' => 'Кількість представників компанії, підприємства (харчування)',

        'step_2_lbl_factory_name' => 'Назва компанії, підприємства',
        'step_2_lbl_pip_dyr' => 'ПІП керівника',
        'step_2_lbl_typ_osoby' => 'Тип особи',
        'step_2_lbl_mob_num_dyr' => 'Номер телефону керівника',
        'step_2_lbl_tel_fax' => 'Тел./факс компанії, підприємства',
        'step_2_lbl_pids_diy' => 'Підстава дії керівника',
        'step_2_lbl_region' => 'Область',
        'step_2_lbl_district' => 'Район',
        'step_2_lbl_city' => 'Населений пункт',
        'step_2_lbl_street' => 'Вулиця буд. кв. ',
        'step_2_lbl_post_index' => 'Поштовий індекс',
        'step_2_lbl_edrpov' => 'Код ЄДРПОУ',
        'step_2_lbl_ipn' => ' ІПН',
        'step_2_lbl_numb_svid_pdv' => 'Номер витягу (свідотцтво платника податку)',
        'step_2_lbl_roz_rah' => 'Р/р',
        'step_2_lbl_bank' => 'Назва банку',
        'step_2_lbl_mfo' => 'МФО Банку',

        'label_pip_vid' => 'ПІП уповноваженого',
        'label_email' => 'Адреса для електронного листування',
        'label_posada' => 'Посада уповноваженого',
        'label_mob_num' => 'Номер телефону',

        'label_logo' => 'Логотип компані',
        'label_logo_info' => 'Логотип можна завантажити у форматі.pdf .eps .cdr .ai, розміром до 7 Mb. Відповідні файли вказаних форматів можна завантажити також заархівованими у форматах .zip .rar',

        'step_3_panel_title_vid' => 'Уповноважена особа',
        'step_3_panel_title_guest' => 'Представника компанії, підприємства',

        'step_3_panel_info_vid' => 'Хто така уповноважена особа відвідувача?',
        'step_3_panel_info_guest' => 'Хто такі представники компанії, підприємства-учасника?',

        'btn_add_guest_caption' => 'Додати представника',
        
        'label_email_guest' => 'Адреса для електронного листування',
        'label_mob_number_guest' => 'Номер телефону',
        'label_posada_guest' => 'Посада представника компанії, підприємства',
        'label_pip_guest' => 'ПІП представника компанії, підприємства',
        
        'step_4_panel_title_info_result' => 'Підсумок реєстрації',
        'step_4_pack_services' => 'Ваш пакет послуг',
        'step_4_panel_title_info_factory' => 'Іфнормація про уповноважену особу',
        'step_4_panel_title_info_dogovyr' => 'Реквізити для договору',
        'step_4_panel_title_info_guest' => 'Перелік гостей від компанії, підприємства',

        "result_label_count_guest" => 'Загальна кількість гостей'
    ]

); 