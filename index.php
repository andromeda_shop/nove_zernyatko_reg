<?php
/**
 * Created by PhpStorm.
 * User: i.yuzkiv
 * Date: 10.01.2019
 * Time: 15:07
 */

session_start();

include "vendor/autoload.php";
use core\Router_XML;
define("ROOT", $_SERVER['DOCUMENT_ROOT']."/");


if (_DEBUG == true) {
    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
}else {
    ini_set('error_reporting', E_NOTICE);
    ini_set('display_error',0);
    ini_set('display_startup_errors', 0);
}



if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    $ip = $_SERVER['HTTP_CLIENT_IP'];
} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
} else {
    $ip = $_SERVER['REMOTE_ADDR'];
}

define("_USER_IP", $ip);


$admin_ip = array(
    "5.58.202.113",
    "95.134.188.66"
);

if (preg_match("/\\/nz_admin\\/\\.*/", $_SERVER['REQUEST_URI']))
{

    if (in_array($ip, $admin_ip))
    {
        $router = new nz_admin\core\Router_XML($_SERVER["DOCUMENT_ROOT"]."/nz_admin/config/routing/routing.xml");
        $data = $router->get($router->getUri());
        $router->run($data);

    }else {
        header("Location: /404"); exit;
    }

}else {
    $router = new Router_XML($_SERVER["DOCUMENT_ROOT"]."/config/routing/routing.xml");
    $data = $router->get($router->getUri());
    $router->run($data);
}




