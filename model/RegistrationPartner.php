<?php
/**
 * Created by PhpStorm.
 * User: i.yuzkiv
 * Date: 05.02.2019
 * Time: 10:42
 */

namespace module;


use core\BaseModel;
use nz_admin\model\Mail;
use nz_admin\model\NameCase;

class RegistrationPartner extends BaseModel
{

    /**
     * @var array
     * Інформація для таблиці partner_main
     */
    private $data_vid = array();
    /**
     * @var array
     * Інформація для таблиці partner_item
     */
    private $data_guest = array();
    /**
     * @var array
     * Інформація для таблиці partner_dogovyr
     */
    private $data_dogovyr = array();


    /**
     * @param array $post_data
     * @return array|bool
     *
     * Перевірка реквізитів для договору на дублікати
     */
    public function validateRegDataStep_1 (Array $post_data)
    {
        unset($post_data['recaptcha_response']);
        $post_data = $this->prepare_data_array($post_data);
        //OR `numb_svid_pdv` = ".$post_data['numb_svid_pdv']."
        $check = $this->db
            ->CreateQuery("SELECT `id` FROM `partner_dogovyr` WHERE `edrpov` = ".$post_data['edrpov']." OR `ipn` = ".$post_data['ipn']." OR `roz_rah` = ".$post_data['roz_rah']." OR `name_factory` LIKE '".$post_data['name_factory']."';");

        $check1 = $this->db
            ->CreateQuery("SELECT `id` FROM `guests_main` WHERE `name_factory` LIKE '".$post_data['name_factory']."';");


        if (empty($check) and empty($check1))
        {
            $_SESSION['registration_info']['step_1'] = $post_data;
            return true;
        }
        else {
            return $this->config->get("msg_error_duplicates");
        }
        

    }

    /**
     * @param array $post_data
     * @return array|bool
     *
     * Перевірка на дублікати по пошті, номеру телефона, ПІБ
     * в бізі таблиці гостей і відповідальних !ОБОХ РЕЄСТРАЦІЙ
     * в самому масиві гостей і відповідального
     * виконую трім
     * якщо є помилки вертаю помиклу
     *
     * Якщо $is_valid_for_guest_reg = true то це провірка для реєстрації гостей
     */
    public function validateRegDataStep_2 (Array $post_data, $is_valid_for_guest_reg = false) {


        $gusets_for_valid = array();

        //Перевіряю на той випадок якщо гостей немає, тобто reg_form_step_2_guest пустий
        if (isset($post_data['reg_form_step_2_guest'])){
            $gusets_for_valid = $post_data['reg_form_step_2_guest'];
        }else $post_data['reg_form_step_2_guest'] = array();


        // Обєдную відповідального з масивом з гостями щоб зменьшити провірку
        $gusets_for_valid[] = array(
            'pip_guest' => $post_data['pip_v'],
            'email_guest' => $post_data['email_v'],
            'mob_num_guest' => $post_data['mob_v']

        );

        $check_duplicates = $this->chackDublInArrayFormStep2($gusets_for_valid);
        if ( is_array($check_duplicates)) {
            return $check_duplicates;
        }else {

            $error = [];

            foreach ($gusets_for_valid as $key => $item) {

                //partner_item
                $check1 = $this->db->CreateQuery("SELECT * FROM `partner_item` WHERE `pip_guest`= '".$item['pip_guest']."' OR `email_guest` = '".$item['email_guest']."' OR `mob_num_guest` = '".$item['mob_num_guest']."';");
                //partner_main
                $check2 = $this->db->CreateQuery("SELECT * FROM `partner_main` WHERE `pip_v`= '".$item['pip_guest']."' OR  `email_v` = '".$item['email_guest']."' OR `mob_v` = '".$item['mob_num_guest']."'");
                //guests_item
                $check3 = $this->db->CreateQuery("SELECT * FROM `guests_item` WHERE `pip_guest`= '".$item['pip_guest']."' OR `email_guest` = '".$item['email_guest']."' OR `mob_num_guest` = '".$item['mob_num_guest']."';");
                //guests_main
                $check4 = $this->db->CreateQuery("SELECT * FROM `guests_main` WHERE `pip_v`= '".$item['pip_guest']."' OR  `email_v` = '".$item['email_guest']."' OR `mob_v` = '".$item['mob_num_guest']."'");

                if (!empty($check1) OR !empty($check2) OR !empty($check3) OR !empty($check4))
                {
                    $error[] = $this->config->get("msg_error_duplicates_step2")/*." `".$item['email_guest']."`"*/;
                }
                if (!empty($error)) break;
            }

            if (empty($error))
            {
                $data_guest = array();

                foreach ($post_data['reg_form_step_2_guest'] as $item) {
                    $data_guest[] = $this->prepare_data_array($item);
                }

                $this->data_guest = $data_guest;

                unset($post_data['reg_form_step_2_guest']);
                $data_vid =  $this->prepare_data_array($post_data);

                $this->data_vid = $data_vid;

                if (!$is_valid_for_guest_reg)
                    $this->data_vid['id_package_services'] = $_SESSION['registration_info']['select_package_services'];

                $this->data_dogovyr = (!$is_valid_for_guest_reg) ? $_SESSION['registration_info']['step_1'] : array();

            }

            return (!empty($error)) ? $error : true;
        }

    }

    private function chackDublInArrayFormStep2 (Array $post_data) {
        $data = array();
        $error = array();

        foreach ($post_data as $key => $item)
        {
            $data[] = $this->prepare_data_array($item);
        }

        if (count($data) > 1)
        {
            for ($i = 0; $i < count($data); $i++)
            {
                if ($i != count($data)-1)
                {
                    if (($data[$i]['email_guest'] == $data[$i+1]['email_guest']))
                        $error[] = $this->config->get("reg_step_2_error_duplicates_email")." Поштовий адрес '".$data[$i]['email_guest']."' уже існує.";
                    if (($data[$i]['mob_num_guest'] == $data[$i+1]['mob_num_guest']))
                        $error[] = $this->config->get("reg_step_2_error_duplicates_phone_numbers")." Номер телефону '".$data[$i]['mob_num_guest']."' уже існує.";

                }else {
                    if (($data[$i]['email_guest'] == $data[0]['email_guest']))
                        $error[] = $this->config->get("reg_step_2_error_duplicates_email")." Поштовий адрес '".$data[$i]['email_guest']."' уже існує.";;
                    if (($data[$i]['mob_num_guest'] == $data[0]['mob_num_guest']))
                        $error[] = $this->config->get("reg_step_2_error_duplicates_phone_numbers")." Номер телефону '".$data[$i]['mob_num_guest']."' уже існує.";;
                }

                if (!empty($error)) break;
            }
        }
        return (!empty($error)) ? $error : true;
    }

    /**
     * @param array $_post_data Тільки одновимірний масив (вкладені масив ні, перебирати циклом)
     * @param string $table_name
     * @return array
     * Видаляю лишні поля з масиву, робилось для Internet Explorer
     * Бо він додавав в POST Лишні поля
     */
    public function comparePostFieldAndDb($_post_data = array(), $table_name = '') {

        $db_data = $this->db->CreateQuery("DESCRIBE `$table_name`");
        $db_field = array();
        foreach ($db_data as $itemDbData) {
            $db_field[] = $itemDbData['Field'];
        }

        foreach ($_post_data as $key => $item)
        {
            if (!in_array($key, $db_field))
            {
                unset($_post_data[$key]);
            }else continue;
        }

        return $_post_data;
    }
    
    /**
     * @return bool
     * Виконую збереження всієї інформації на протяці реєстрації
     */
    public function savePartner() {

        if (!empty($this->data_dogovyr) AND !empty($this->data_vid))
        {

            /**
             * Видаляю лишні поля з масиву, робилось для Internet Explorer
             * Бо він додавав в POST Лишні поля
             */
            $this->data_dogovyr = $this->comparePostFieldAndDb($this->data_dogovyr, 'partner_dogovyr');
            $this->data_vid = $this->comparePostFieldAndDb($this->data_vid, 'partner_main');

            $id = $this->db->insert_get_id($this->data_vid, "partner_main");

            if (is_integer(intval($id)) and $id != 0 and $id != NULL)
            {
                $this->data_dogovyr['id_partner_main'] = $id;

                $this->db->insert($this->data_dogovyr, "partner_dogovyr");

                if (!empty($this->data_guest)) //Перевіряю на той випадок якщо гостей немає, тобто data_guest пустий
                {
                    foreach ($this->data_guest as $item)
                    {
                        $item = $this->comparePostFieldAndDb($item, 'partner_item');
                        $item['id_partner_main'] = $id;
                        $this->db->insert($item, "partner_item");
                    }
                }

                unset($_SESSION['registration_info']);
                unset($this->data_dogovyr);
                unset($this->data_guest);
                unset($this->data_vid);

                $_SESSION['id_partner'] = $id;
                return true;
            }else {
                return false;
            }
        }else return false;
    }

    /**
     * @param array $FILE
     * @return bool
     * Збереження файлу на сервер
     */
    public function saveLogo (Array $FILE) {

        if (!empty($FILE) AND isset($FILE['logo-file']['name']) AND isset($_SESSION['id_partner'])) {
            $uploaddir = 'files/logo_company/';

            if(move_uploaded_file($FILE['logo-file']['tmp_name'], $uploaddir . $_SESSION['id_partner'].substr($FILE['logo-file']['name'], -4)))
            {
                return true;
            }else
                return false;
        }return false;

    }

    /**
     * @param $id
     * @return array
     * Підсумкова інфомрації про реєстранта
     */
    public function getDataForStepFour($id) {


        $data = array();

        $data['info_main'] = $this->db
            ->setSelect("*")
            ->setFrom("partner_main")
            ->setWhere("id=".$id)
            ->one();
        $info_guests = $this->db
            ->setSelect("*")
            ->setFrom("partner_item")
            ->setWhere("`id_partner_main`= ".$id)
            ->all();



        $vidpovid = array(
            'pip_guest' => $data['info_main']['pip_v'],
            'mob_num_guest' => $data['info_main']['mob_v'],
            'email_guest' => $data['info_main']['email_v'],
            'posada_guest'=> $data['info_main']['posada_v'],
        );
        //Перевіряю на той випадок якщо гостей немає, тобто $info_guests пустий
        
        if (empty($info_guests) or !isset($info_guests[0]))
        {
            $info_guests[0] = $vidpovid;
        }else {
            array_unshift($info_guests, $vidpovid);
        }

        $data['info_guests'] = $info_guests;
        $data['info_dogovyr'] = $this->db
            ->setSelect("*")
            ->setFrom("partner_dogovyr")
            ->setWhere("id_partner_main=".$id)
            ->all();


        $data['diyalnist'] = "";

        foreach (explode(",",$data['info_main']['diyalnist_id']) as $item){
            $data['diyalnist'] .= ", ".$this->db->setSelect("name")->setFrom("diyalnist_item")->setWhere("id=".$item)->one()['name'];
        }

        $data['diyalnist'] = substr($data['diyalnist'], 2);

        $data['package_services'] = $this->db->setSelect("*")->setFrom("package_services")->setWhere("id=".$data['info_main']['id_package_services'])->one();
        $data['package_services']['other'] = explode(";", $data['package_services']['other']);

        return $data;
        
    }


    public function saveVisitors () {

        if (!empty($this->data_vid))
        {

            $this->data_vid = $this->comparePostFieldAndDb($this->data_vid, 'guests_main');

            $id = $this->db->insert_get_id($this->data_vid, "guests_main");

            if (is_integer(intval($id)) and $id != 0 and $id != NULL)
            {
                if (!empty($this->data_guest)) //Перевіряю на той випадок якщо гостей немає, тобто data_guest пустий
                {
                    foreach ($this->data_guest as $item)
                    {
                        $item = $this->comparePostFieldAndDb($item, 'guests_item');
                        $item['id_guest_main'] = $id;
                        $this->db->insert($item, "guests_item");
                    }
                }

                unset($this->data_guest);
                unset($this->data_vid);

                $_SESSION['id_visitors'] = $id;
                return true;
            }else {
                return false;
            }

        }else return false;
    }

    /**
     * @param int $type
     * @param string $recipients
     * @param $main_id
     * Стартове повідомлення після реєстрації
     */
    public function sendMail($type = 1, $recipients = "", $main_id) {

        $mail = new Mail();
        $mail->setRecipients([$recipients])
            ->setMailContentSubject("Реєстрація на Ⅳ-й Міжнародний Форум аграрних інновацій «Нове Зернятко»");

        $table_update = "partner_main";

        switch ($type)
        {
            case 1: //учасник

                $table_update = "partner_main";

                $mail->setMailContentBody("<div> <h3>Доброго дня.</h3> <h3>Ви зареєструвались як <strong>учасник</strong> Ⅳ-го Міжнародного Форуму аграрних інновацій «Нове Зернятко» - Настасів 2019.</h3> <h3>Дякуємо!</h3> </div>");
                $mail->setMailContentAltBody("Доброго дня. Ви зареєструвались як учасник 4-го Міжнародного Форуму аграрних інновацій «Нове Зернятко» - Настасів 2019. Дякуємо!");
                break;
            case 2: //відвідувач

                $table_update = "guests_main";

                $mail->setMailContentBody("<div> <h3>Доброго дня.</h3> <h3>Ви зареєструвались як <strong>відвідувач</strong> Ⅳ-го Міжнародного Форуму аграрних інновацій «Нове Зернятко» - Настасів 2019.</h3> <h3>Дякуємо!</h3> </div>");
                $mail->setMailContentAltBody("Доброго дня. Ви зареєструвались як відвідувач 4-го Міжнародного Форуму аграрних інновацій «Нове Зернятко» - Настасів 2019. Дякуємо!");
                break;
        }

        $user_status = $this->db->setSelect("status")->setFrom($table_update)->setWhere("id=".$main_id)->one();

        if ($user_status != NULL AND $user_status['status'] == 0)
        {
            if ($mail->sendMail())
            {
                $this->db->update(['status' => 1], $table_update, "id=".$main_id);
            }else {
                $this->db->update(['status' => 2], $table_update, "id=".$main_id);
            }
        }else {
            $mail->writeLog("Повідомлення не відправленно, користувачу уже відправленно стартове повідомлення або такого користувач нема в таблиці - ".$table_update);
        }

    }

    
}