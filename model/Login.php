<?php
/**
 * Created by PhpStorm.
 * User: i.yuzkiv
 * Date: 16.03.2019
 * Time: 10:46
 */

namespace module;


use core\BaseModel;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use nz_admin\model\UpdateRegData;

class Login extends BaseModel
{

    /**
     * @var array
     */
    private $loginData = array();
    /**
     * @var
     * 0 - учасник
     * 1 - відвідувач
     */
    private $type = null;
    /**
     * @var null
     */
    private $id_main = null;
    /**
     * @var string
     */
    private $table_name = '';
    /**
     * @var Logger
     */
    private $log;
    /**
     * @var string
     * File dogovir save dir
     */
    private $dogovyr_save_dir = "files/company_dogovyr/";
    /**
     * @var string
     * File dogovir format
     */
    private $dogovyr_format = ".pdf";


    /**
     * Login constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->log = new Logger("LK");
        $this->log->pushHandler(new StreamHandler('log/lk.log'));
    }

    /**
     * @param array $loginData
     * @return bool|string
     */
    public function validLogin (Array $loginData){
        if(!empty($loginData)) {

            $this->loginData = $loginData = $this->prepare_data_array($loginData);

            $this->type = $loginData['type'];

            $table = "";
            switch ($this->type) {
                case 0:
                    $this->table_name = $table = "partner_lk";
                    break;
                case 1:
                    $this->table_name = $table = "guests_lk";
                    break;
            }

            $user_data = $this->db->setSelect('*') -> setFrom($table) ->setWhere(" `email` = '".trim($this->loginData['loginLK_form_login'])."' ")->one();

            if ($user_data == null or empty($user_data) )
            {
                $user_data = $this->getNumForLogin($this->loginData['loginLK_form_login']);
            }

            if ($user_data == null)
            {
                $this->log->error("НЕ АВТОРИЗОВАНО >> ip: "._USER_IP.", login: ".$this->loginData['loginLK_form_login'].", table: ".$this->table_name);
                return "login_user_error";
            }else {
                if ($user_data['password'] == $this->loginData['loginLK_form_password'])
                {
                    $this->id_main = $user_data['id'];
                    $this->onSuccessLogin();
                    $this->log->info("АВТОРИЗОВАНО >> ip: "._USER_IP.", login: ".$this->loginData['loginLK_form_login'].", table: ".$this->table_name);
                    return true;
                }
                else {
                    return "login_user_error";
                }
            }

        }else {
            return false;
        }
    }


    /**
     * @param null $search_number
     * @return array|null
     *
     * Якщо search_number = null вертає всіх з таблиці з підготовленними номерами
     */
    public function getNumForLogin ($search_number = NULL) {

        //забираю лишін символи з номера
        $search_number = preg_replace("/[^,.0-9]/", '', $search_number);

        $lk = null;
        if ($this->type == 0)
        {
            $lk = $this->db->setSelect("*")->setFrom("partner_lk")->all();
        }elseif ($this->type == 1)
        {
            $lk = $this->db->setSelect("*")->setFrom("partner_lk")->all();
        }

        $result = null;
        foreach ($lk as $key => $item) {
            $lk[$key]['mob'] = $item['mob'] = preg_replace("/[^,.0-9]/", '', $item['mob']);

            if ($search_number != NULL AND $item['mob'] == $search_number)
            {
                $result = $item;
                break;
            }
        }

        return ($search_number != NULL) ? $result : $lk;
    }

    /**
     * Виконується після успішної авторизації
     */
    private function onSuccessLogin () {

        $this->db->update([
            'last_login' => date("Y-m-d"),
            'last_ip' => _USER_IP,
            'session_token' => generateToken(false)
        ], $this->table_name, 'id='.$this->id_main);

        $session = $this->db->setSelect('*')->setFrom($this->table_name)->setWhere("id=".$this->id_main)->one();
        unset($session['password'], $session['last_login'], $session['last_ip']);
        $session['type'] = $this->type;
        if ($session != null)
        {
            $_SESSION['lk_user_info'] = $session;
        }

    }

    /**
     * @return bool
     * Перевірка чи авторизований користувач
     */
    public function checkAuthorized() {

        if (!isset($_SESSION['lk_user_info']) or empty($_SESSION['lk_user_info'])) return false;
        $session_info = $_SESSION['lk_user_info'];

        $table_name = "";
        switch ($session_info['type']) {
            case 0:
                $table_name = "partner_lk";
                $filed_name = 'id_partner_main';
                break;
            case 1:
                $table_name = "guests_lk";
                $filed_name = 'id_guest_main';
                break;
        }

        $db_info = $this->db
            ->setSelect("id, $filed_name, email, mob, status, session_token")
            ->setFrom($table_name)
            ->setWhere('id='.$session_info['id'])
            ->one();

        if ($db_info == NULL OR empty($db_info)) return false;

        if ($db_info['session_token'] == $session_info['session_token'])
        {
            return true;
        }else {
            return false;
        }
    }

    /**
     * @return bool|array
     */
    public function getUserInfo () {

        if (!isset($_SESSION['lk_user_info']) or empty($_SESSION['lk_user_info'])) return false;
        $session_info = $_SESSION['lk_user_info'];

        switch ($session_info['type']) {
            case 0:
                $table_name = "partner";
                $filed_name = 'id_partner_main';
                $type = 'partner';
                $id = $session_info['id_partner_main'];

                $dogovyr_data = $this->db->setSelect("*")->setFrom('partner_dogovyr')->setWhere('id_partner_main='.$session_info['id_partner_main'])->one();

                break;
            case 1:
                $table_name = "guests";
                $filed_name = 'id_guest_main';
                $type = 'guests';
                $id = $session_info['id_guest_main'];
                break;
        }

        $result['dogovyr_info'] = (isset($dogovyr_data)) ? $dogovyr_data : null;

        $result['base_info'] = $this->db->setSelect('*') -> setFrom($table_name.'_main') -> setWhere("id=".$session_info[$filed_name]) ->one();
        $result['guest_info'] = $this->db->setSelect('*') -> setFrom($table_name.'_item') -> setWhere($filed_name."=".$session_info[$filed_name]) ->all();
        $result['file_info'] = array();
        $result['base_info']['count_guest'] = ($result['guest_info'] != NULL OR !empty($result['guest_info'])) ? count($result['guest_info']) : 0;

        if($session_info['type'] == 0)
        {
            $result['package_services_info'] = $this->db
                ->setSelect("*")
                ->setFrom("package_services")
                ->setWhere("id=".$result['base_info']['id_package_services'])
                ->one();

            $result['package_services_info']['other'] = explode(';',$result['package_services_info']['other']);

            $result['file_info']['dogovyr'] = (file_exists($this->dogovyr_save_dir.$id.$this->dogovyr_format)) ? $this->dogovyr_save_dir.$id.$this->dogovyr_format : "";


        }

        $result['file_info']['ticket'] = file_exists("files/tickets/$type/$id.pdf") ? "files/tickets/$type/$id.pdf" : "";

        return $result;
    }

}