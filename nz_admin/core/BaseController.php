<?php
/**
 * Created by PhpStorm.
 * User: bood92
 * Date: 24.11.2018
 * Time: 17:12
 */

namespace nz_admin\core;


use core\BaseView;
use core\config;
use core\Database;
use core\Registry;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use nz_admin\model\Login;

class BaseController
{
    /**
     * @var \Smarty
     */
    protected $smart;
    /**
     * @var Database
     */
    protected $db;
    /**
     * @var config
     */
    protected $config;
    /**
     * @var Registry
     */
    protected $Register;

    protected $hashField = "";
    protected $hashFieldLen;
    private $file_hash_data = "asdasdas2dd_hash_field";

    /**
     * @var BaseView
     */
    protected $View;

    protected  $log;

    protected $proviryaty_login = true; 

    /**
     * BaseController constructor.
     */
    public function __construct() {
        $this->Register = Registry::getInstance();
        $this->smart = $this->Register->get("Smarty");
        $this->db = $this->Register->get("_DB_");
        $this->config = $this->Register->get("_CONFIG_");
        $this->View = new \nz_admin\core\BaseView();
        $this->getHashField();
        $this->log = new Logger("nz_admin_log/");
        $this->log->pushHandler(new StreamHandler("nz_admin/admin_log/log.log"));


        if ($this->proviryaty_login == true) {
            $this->checkLogin();
        }

    }

    protected function getHashField() {
        $hash_data = explode(",",file_get_contents($this->file_hash_data));
        $this->hashFieldLen = $hash_data[0];
        $this->hashField = $hash_data[1];
    }
    protected  function validHashField($_post_data="") {
        if ($_post_data != "" or $_post_data != NULL)
        {
            if (mb_strlen($_post_data) !=  $this->hashFieldLen)
            {
                return false;
            }else return true;

        }else return false;
    }

    private function checkLogin() {
        if (isset($_SESSION['admin_user']))
        {
            $mng_login = new Login();
            $check = $mng_login->getUserById($_SESSION['admin_user']['user_id']);
            if ($check['token'] == $_SESSION['admin_user']['token'])
            {
                return true;
            }else $this->redirect404();
        } else $this->redirectToUrl("/nz_admin/login/");
    }

    /**
     *
     */
    protected function redirect404 () {
        echo '<script type="application/javascript"> location.href="/404" </script>';
    }

    /**
     * @param $url
     */
    protected function redirectToUrl($url) {
        if ($url != NULL)
        {
            echo '<script type="application/javascript"> location.href="'.$url.'" </script>';
        }
    }

}