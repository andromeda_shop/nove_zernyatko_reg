<?php
/**
 * Created by PhpStorm.
 * User: bood92
 * Date: 25.11.2018
 * Time: 14:03
 */

namespace nz_admin\core;

use core\{
    config,
    Database,
    Registry
};

class BaseView
{
    /**
     * @var \Smarty
     */
    protected $smart;
    /**
     * @var Database
     */
    protected $db;
    /**
     * @var config
     */
    protected $config;
    /**
     * @var Registry
     */
    protected $Register;

    /**
     * @var array
     * View system option
     */
    protected $_view_system_option = [
        '_URL'                  =>
            [
                'view'              => _VIEW_ADMIN,
                'form_field_tpl'    => _VIEW_ADMIN.'_system/_field_form_group/',
                'system'            => _VIEW_ADMIN.'_system/',
                'image'            => '/nz_admin/view/_system/image/'
            ],
        'scripts'               => [
            "https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js",
            "/view/_system/js/bootstrap.min.js",
            "/nz_admin/view/_system/js/scripts.js",

        ],
        'styles'                => [
            "/view/_system/css/new_bootstrap.min.css",
            "/nz_admin/view/_system/css/left_menu.css",
            "/nz_admin/view/_system/css/main.css",
            "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css",
            "https://fonts.googleapis.com/css?family=Orbitron",

            
        ],
        'title'                 => "Title",
        'h1_title'              => "",

    ];

    protected $_base_menu = [
        [
            'title' => 'Реєстрація',
            'alt' => '',
            'alias' => 'registration',
            'href' => '/',
            'active' => 0
        ],
        [
            'title' => 'Особистий кабінет',
            'alt' => '',
            'alias' => 'login',
            'href' => '/login',
            'active' => 0
        ],
        [
            'title' => 'Питання відповідь',
            'alt' => '',
            'alias' => 'faq',
            'href' => '/faq',
            'active' => 0
        ]
    ];

    public $USER_STYLES = [];
    public $USER_SCRIPTS = [];

    public $current_view;

    /**
     * BaseController constructor.
     */
    public function __construct()
    {
        $this->Register = Registry::getInstance();
        $this->smart = $this->Register->get("Smarty");
        $this->db = $this->Register->get("_DB_");
        $this->config = $this->Register->get("_CONFIG_");

    }

    /**
     * @param string $view
     * @param array $params
     */
    public function render ($view = "", $params = []) {

        $this->smart->assign("_CURRENT_VIEW", $view);

        $view = preg_replace("/.html/", "", strtolower($view));
        $params['view_system_option'] = $this->_view_system_option;
        $params['view_system_option']['include_view'] = _VIEW_ADMIN.$view.".html";
        $params['view_system_option']['base_menu'] = $this->_base_menu;

        if (!empty($params)) {
            foreach ($params as $name => $value) {
                $this->smart->assign($name, $value);
            }
        }
        $this->smart->display(_VIEW_ADMIN."_system/index.html");
    }

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle ($title = "") {
        $this->_view_system_option['title'] = $title." | Адмін панель";
        return $this;
    }

    /**
     * @param string $h1_title
     * @return $this
     */
    public function setH1Title ($h1_title = "") {
        $this->_view_system_option['h1_title'] = $h1_title;
        return $this;
    }

    /**
     * @param string $alias
     * @return $this
     */
    public function setActiveMenu ($alias="") {

        for ($i = 0; $i < count($this->_base_menu); $i++ )
        {
            if ($this->_base_menu[$i]['alias'] == $alias)
            {
                $this->_base_menu[$i]['active'] = 1;
            }else{
                $this->_base_menu[$i]['active'] = 0;
            }
        }

        return $this;
    }

    /**
     * @param array $Styles
     * @return $this
     */
    public function setUserStyles ($Styles = []) {
        if ( !empty($Styles) ) {
            foreach ($Styles as $item) {
                $this->_view_system_option['styles'][] = $item;
            }
        }

        return $this;
    }

    /**
     * @param array $Scripts
     * @return $this
     */
    public function setUserScripts ($Scripts = [])
    {
        if ( !empty($Scripts) ) {
            foreach ($Scripts as $item) {
                $this->_view_system_option['scripts'][] = $item;
            }
        }


        return $this;
    }

}



/*
{$view_system_option|@dump}
-----------------------
Array
(
    [_URL] => Array
        (
            [view] => nz_admin/view/
            [form_field_tpl] => nz_admin/view/_system/_field_form_group/
            [system] => nz_admin/view/_system/
            [image] => /nz_admin/view/_system/image/
        )

    [scripts] => Array
        (
            [0] => https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js
            [1] => /view/_system/js/bootstrap.min.js
            [2] => /nz_admin/view/_system/js/scripts.js
        )

    [styles] => Array
        (
            [0] => /view/_system/css/new_bootstrap.min.css
            [1] => /nz_admin/view/_system/css/left_menu.css
            [2] => /nz_admin/view/_system/css/main.css
            [3] => https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css
            [4] => https://fonts.googleapis.com/css?family=Orbitron
        )

    [title] => ТОВ "АГРОХІМТЕХНОЛОДЖІ" | Адмін панель
    [h1_title] => Інфомрація про учасника форуму ТОВ "АГРОХІМТЕХНОЛОДЖІ" ( id = 40)
    [include_view] => nz_admin/view/partners/partner_item.html
    [base_menu] => Array
        (
            [0] => Array
                (
                    [title] => Реєстрація
                    [alt] =>
                    [alias] => registration
                    [href] => /
                    [active] => 0
                )

            [1] => Array
                (
                    [title] => Особистий кабінет
                    [alt] =>
                    [alias] => login
                    [href] => /login
                    [active] => 0
                )

            [2] => Array
                (
                    [title] => Питання відповідь
                    [alt] =>
                    [alias] => faq
                    [href] => /faq
                    [active] => 0
                )

        )

)
-----------------------
*/