/**
 * Created by bood92 on 10.02.2019.
 */

function showMsgError (textError, headerText) {
    $("#errorBoxModal").find(".modal-body").html(textError);

    if (headerText != "" || headerText != undefined || headerText != null)
    {
        $("#errorBoxModal").find(".modal-header span.headerError").html(headerText);
    }

    $("#errorBoxModal").modal();
}


/*Table Filter*/

$(".TableFilterField tbody tr").on("click", function () {

    if ($(this).attr("data-href") != undefined) {
        location.href = $(this).attr("data-href");
    }


});

/*END Table Filter*/


$(document).ready(function () {

    /*NAV BAR*/
    var trigger = $('.hamburger'),
        overlay = $('.overlay'),
        isClosed = false;

    trigger.click(function () {
        hamburger_cross();
    });

    function hamburger_cross() {

        if (isClosed == true) {
            overlay.hide();
            trigger.removeClass('is-open');
            trigger.addClass('is-closed');
            isClosed = false;
        } else {
            overlay.show();
            trigger.removeClass('is-closed');
            trigger.addClass('is-open');
            isClosed = true;
        }
    }

    $('[data-toggle="offcanvas"]').click(function () {
        $('#wrapper').toggleClass('toggled');
    });
    /*END NAV BAR*/

    /*AdminInfoTables*/

    $("table.AdminInfoTables tr").click(function () {

        if ($(this).attr("data-href") != undefined)
        {
            location.href = $(this).attr("data-href");
        }

    });
    /*END AdminInfoTables*/
    ON_PAGE_LOAD_1();
});


