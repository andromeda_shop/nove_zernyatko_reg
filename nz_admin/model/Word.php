<?php

/**
 * Created by PhpStorm.
 * User: bood92
 * Date: 10.02.2019
 * Time: 15:43
 */
class Word extends ZipArchive{

    // Файлы для включения в архив
    private $files;

    // Путь к шаблону
    public $path;

    // Содержимое документа
    protected $content;
    
    public function __construct($filename, $template_path = 'files/templates/dog/' ){

        //\core\Registry::getByRef("BaseModel");
        $this->baseModel = new \core\BaseModel();
        // Путь к шаблону
        $this->path = $template_path;

        // Если не получилось открыть файл, то жизнь бессмысленна.
        if ($this->open($filename, ZIPARCHIVE::CREATE) !== TRUE) {
            die("Unable to open <$filename>\n");
        }


        // Структура документа
        $this->files = array(
            "word/_rels/document.xml.rels",
            "word/theme/theme1.xml",
            "word/fontTable.xml",
            "word/settings.xml",
            "word/styles.xml",
            "word/stylesWithEffects.xml",
            "word/webSettings.xml",
            "_rels/.rels",
            "docProps/app.xml",
            "docProps/core.xml",
            "[Content_Types].xml" );

        // Добавляем каждый файл в цикле
        foreach( $this->files as $f )
            $this->addFile($this->path . $f , $f );
    }
    
    // Упаковываем архив
    public function create(Array $search, Array $replace){

        $doc = file_get_contents( $this->path . "word/document.xml" );
        $result = str_replace($search, $replace, $doc);

        $this->addFromString("word/document.xml", $result);
        $this->close();
    }
}


/*
 *
 *  Example

       $replace = [
            'NameFactory' => $dogovyr_info['name_factory'],
            'EDRPOU' => $dogovyr_info['edrpov'],
            'Ad_Index' => $dogovyr_info['post_index'],
            'ADD_CITY' => $dogovyr_info['city'],
            'ADD_DIST' => $dogovyr_info['district'],
            'ADD_STREET' => $dogovyr_info['street'],
            'NUM_Ipn' => $dogovyr_info['ipn'],
            'RozRah' => $dogovyr_info['roz_rah'],
            'bankName' => $dogovyr_info['name_bank'],
            'bankMfo' => $dogovyr_info['mfo'],
            'fax_tel' => $dogovyr_info['fax_tel'],
            'sv_pdv' => $dogovyr_info['numb_svid_pdv'],
            "packSerivces" => "Пакет послуг",
            'DYR_SHORT'     => $dyr_short,
        ];

        $word = new \Word($this->patchUserDogovyr . $id_partner . ".docx", $this->patchToTemplate);
        $word->create(array_keys($replace), array_values($replace));

 */
