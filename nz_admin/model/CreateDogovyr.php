<?php
/**
 * Created by PhpStorm.
 * User: i.yuzkiv
 * Date: 11.02.2019
 * Time: 11:11
 */

namespace nz_admin\model;


use core\BaseModel;
use Mpdf\Mpdf;

class CreateDogovyr extends  BaseModel
{
    /**
     * @param $id_partner
     * @return bool
     * @throws \Mpdf\MpdfException
     */
    public function generateDogovyr($id_partner)
    {
        $mng_get_data = new GetData();
        $dogovyr_info = $mng_get_data->getDataItemDogovyr($id_partner);

        $tmp  = stristr($dogovyr_info['name_factory'], " ", true);
        $nameFactoryWitoutOPF  = substr($dogovyr_info['name_factory'], (strlen($tmp)-strlen($dogovyr_info['name_factory']))+1);

        $pidstavaDiyDyr = [
            'Статут' => "Статуту",
            'Наказ' => "Наказу",
            'Довіреність' => "Довіреністі",
            'Свідоцтво' => "Свідоцтва",
        ];

        $packageServicesList = "";
        foreach (explode(";", $dogovyr_info['package_services_info']['other_dog']) as $item)
        {
            $packageServicesList .= '<li style="text-indent: -18.0pt;"><span style="font-family: \'Times New Roman\',serif;">&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp;&nbsp;';
            $packageServicesList .= $item;
            $packageServicesList .= '</li>';
        }

        $dogovyr_info['posada_dyr'] = mb_strtoupper(mb_substr($dogovyr_info['posada_dyr'], 0, 1)).mb_substr($dogovyr_info['posada_dyr'], 1);

        $replace = [
            'FooterNameFactory'                     => $dogovyr_info['name_factory'],                   // Назва компанії в реквізитах
            'NumberEdrpou'                          => $dogovyr_info['edrpov'],                         // ЕДРПОУ в реквізитах
            'AddressIndex'                          => $dogovyr_info['post_index'],                     // Поштовий індекс в реквізитах
            'AddressCity'                           => $dogovyr_info['city'],                           // Населенний пункт в реквізитах
            'AddressDist'                           => $dogovyr_info['district'],                       // Район в реквізитах
            'AddressRegion'                         => $dogovyr_info['region'],                         // Область в реквізитах
            'AddressStreet'                         => $dogovyr_info['street'],                         // Вулиця в реквізитах
            'NumberIpn'                             => $dogovyr_info['ipn'],                            // ІПН в реквізитах
            'NumberRozRah'                          => $dogovyr_info['roz_rah'],                        // Розрахунковий рахунок в реквізитах
            'BankName'                              => $dogovyr_info['name_bank'],                      // Назва банку в реквізитах
            'BankMfo'                               => $dogovyr_info['mfo'],                            // МФО банку в реквізитах
            'FaxTel'                                => $dogovyr_info['fax_tel'],                        // Факс телефон в реквізитах
            'DyrectorShort'                         => $dogovyr_info['dyr_short'],                      // Прізвище І. П. директора (коротко) в реквізитах
            'NumberSvPlatPdv'                       => $dogovyr_info['numb_svid_pdv'],                  // Номер свідоцтва платника податків в реквізитах
            'shortOrganizaPravForma'                => mb_strtoupper(stristr($dogovyr_info['name_factory'], " ", true)),
            "organizaPravForma"                     => $dogovyr_info['org_pravova_forma'],              // Повна організаційно правова форма
            "NameFactoryWithoutOrganizaPravForma"   => str_replace('"', '', $nameFactoryWitoutOPF),                           // Назва компанії без організацйно правової форми
            "pipDyrNameCase"                        => $dogovyr_info['pip_dyr_vid'],                    // ПІП дирекора в родовому відмінку
            "pidstavaDiyDyr"                        => $pidstavaDiyDyr[$dogovyr_info['pids_diy_dyr']],  // Підстав дії директора
            "packageServicesName"                   => $dogovyr_info['package_services_info']['name'],  //Назва тарифного плану
            "packageServicesList"                   => $packageServicesList,
            "packageServicesPrice"                  => $dogovyr_info['package_services_info']['price'],
            "packageServicesTagPrice"               => $dogovyr_info['package_services_info']['price_tag'],
            "posadaDyrRod"                          => $dogovyr_info['posada_dyr_rod'],
            "posadaDyrFooter"                       => $dogovyr_info['posada_dyr']
        ];

        //dump($replace);

        $replace['FooterNameFactory'] = str_replace('"', '', $replace['FooterNameFactory']);
        $replace['organizaPravForma'] = mb_strtoupper($replace['organizaPravForma']);

        $mng_html_pdf = new HtmlToPdf();
        $mng_html_pdf->setTemplateFile("files/templates/html_dog/dog_1.html");
        $mng_html_pdf->setSaveDir("files/company_dogovyr/");


        if ($dogovyr_info['pip_dyr_vid'] == "" OR $dogovyr_info['org_pravova_forma'] == "" OR $dogovyr_info['pip_dyr_vid'] == NULL OR $dogovyr_info['org_pravova_forma'] == NULL) {
            return false;
        }else {
            $mng_html_pdf->replaceAndCreateManyTemplates(array_keys($replace), array_values($replace), $id_partner, ["files/templates/html_dog/dog_1.html", "files/templates/html_dog/dog_1_dodatok_1.html"]);
            //$mng_html_pdf->replaceAndCreate(array_keys($replace), array_values($replace), $id_partner);


            $this->db->update(['status' => 3], "partner_main", "id=".$id_partner);
            return true;
        }

    }

    public function genSpec($id_partner) {
        $mng_get_data = new GetData();
        $dogovyr_info = $mng_get_data->getDataItemDogovyr($id_partner);

        $tmp  = stristr($dogovyr_info['name_factory'], " ", true);
        $nameFactoryWitoutOPF  = substr($dogovyr_info['name_factory'], (strlen($tmp)-strlen($dogovyr_info['name_factory']))+1);

        $pidstavaDiyDyr = [
            'Статут' => "Статуту",
            'Наказ' => "Наказу",
            'Довіреність' => "Довіреністі",
            'Свідоцтво' => "Свідоцтва",
        ];

        $packageServicesList = "";
        foreach (explode(";", $dogovyr_info['package_services_info']['other_dog']) as $item)
        {
            $packageServicesList .= '<li style="text-indent: -18.0pt;"><span style="font-family: \'Times New Roman\',serif;">&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp;&nbsp;';
            $packageServicesList .= $item;
            $packageServicesList .= '</li>';
        }

        $replace = [
            'FooterNameFactory'                     => $dogovyr_info['name_factory'],                   // Назва компанії в реквізитах
            'NumberEdrpou'                          => $dogovyr_info['edrpov'],                         // ЕДРПОУ в реквізитах
            'AddressIndex'                          => $dogovyr_info['post_index'],                     // Поштовий індекс в реквізитах
            'AddressCity'                           => $dogovyr_info['city'],                           // Населенний пункт в реквізитах
            'AddressDist'                           => $dogovyr_info['district'],                       // Район в реквізитах
            'AddressRegion'                         => $dogovyr_info['region'],                         // Область в реквізитах
            'AddressStreet'                         => $dogovyr_info['street'],                         // Вулиця в реквізитах
            'NumberIpn'                             => $dogovyr_info['ipn'],                            // ІПН в реквізитах
            'NumberRozRah'                          => $dogovyr_info['roz_rah'],                        // Розрахунковий рахунок в реквізитах
            'BankName'                              => $dogovyr_info['name_bank'],                      // Назва банку в реквізитах
            'BankMfo'                               => $dogovyr_info['mfo'],                            // МФО банку в реквізитах
            'FaxTel'                                => $dogovyr_info['fax_tel'],                        // Факс телефон в реквізитах
            'DyrectorShort'                         => $dogovyr_info['dyr_short'],                      // Прізвище І. П. директора (коротко) в реквізитах
            'NumberSvPlatPdv'                       => $dogovyr_info['numb_svid_pdv'],                  // Номер свідоцтва платника податків в реквізитах
            'shortOrganizaPravForma'                => mb_strtoupper(stristr($dogovyr_info['name_factory'], " ", true)),
            "organizaPravForma"                     => $dogovyr_info['org_pravova_forma'],              // Повна організаційно правова форма
            "NameFactoryWithoutOrganizaPravForma"   => str_replace('"', '', $nameFactoryWitoutOPF),                           // Назва компанії без організацйно правової форми
            "pipDyrNameCase"                        => $dogovyr_info['pip_dyr_vid'],                    // ПІП дирекора в родовому відмінку
            "pidstavaDiyDyr"                        => $pidstavaDiyDyr[$dogovyr_info['pids_diy_dyr']],  // Підстав дії директора
            "packageServicesName"                   => $dogovyr_info['package_services_info']['name'],  //Назва тарифного плану
            "packageServicesList"                   => $packageServicesList,
            "packageServicesPrice"                  => $dogovyr_info['package_services_info']['price'],
            "packageServicesTagPrice"               => $dogovyr_info['package_services_info']['price_tag'],
            "posadaDyrRod"                          => $dogovyr_info['posada_dyr_rod'],
            "posadaDyrFooter"                       => $dogovyr_info['posada_dyr']
        ];

        $replace['FooterNameFactory'] = str_replace('"', '', $replace['FooterNameFactory']);

        $replace['organizaPravForma'] = mb_strtoupper($replace['organizaPravForma']);

        $mng_html_pdf = new HtmlToPdf();
        $mng_html_pdf->setTemplateFile("files/templates/html_dog/dog_1_dodatok_1.html");
        $mng_html_pdf->setSaveDir("files/company_dogovyr/cert/");


        if ($dogovyr_info['pip_dyr_vid'] == "" OR $dogovyr_info['org_pravova_forma'] == "" OR $dogovyr_info['pip_dyr_vid'] == NULL OR $dogovyr_info['org_pravova_forma'] == NULL) {
            return false;
        }else {
            //$mng_html_pdf->replaceAndCreateManyTemplates(array_keys($replace), array_values($replace), $id_partner, ["files/templates/html_dog/dog_1.html", "files/templates/html_dog/dog_1_dodatok_1.html"]);

            $mng_html_pdf->replaceAndCreate(array_keys($replace), array_values($replace), $id_partner);
            $this->db->update(['status' => 3], "partner_main", "id=".$id_partner);
            return true;
        }
    }

    /**
     * @param $id
     * @param bool $if_null
     * @return array|bool
     * Розшифровка організаційно правоої форми
     */
    public function set_organiza_prav_forma ($id, $if_null=false ) {
        $item_data = $this->db
            ->setSelect("name_factory")
            ->setFrom("partner_dogovyr")
            ->setWhere("id_partner_main=".$id)
            ->one();

        $org_prav_form = $this->config->get("org_prav_form");
        $result = NULL;

        if ($item_data != NULL)
        {
            foreach ($org_prav_form as $item) {
                //stristr - вертає підстроку до першого входження символу
                if (mb_strtoupper($item['name']) == mb_strtoupper(stristr($item_data['name_factory'], " ", true)))
                {
                    $result = $item['val'];
                    break;
                }else continue;
            }
        }

        if ($result != NULL OR $result != NULL)
        {
            $where = "id_partner_main=".$id;
            $where .= ($if_null) ? " AND org_pravova_forma = NULL" : " ";

            if ($this->db->update(['org_pravova_forma' => $result], "partner_dogovyr", "id_partner_main=".$id)) {
                return true;
            }else
                return false;

        }else {
            return $this->config->get("msg_error_set_org_prav_forma_1");
        }

    }

}