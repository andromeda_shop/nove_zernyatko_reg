<?php
/**
 * Created by PhpStorm.
 * User: bood92
 * Date: 08.03.2019
 * Time: 18:05
 */

namespace nz_admin\model;


use core\BaseModel;
use nz_admin\model\GetData;
use nz_admin\model\HtmlToPdf;

class CreateTicket extends BaseModel
{

    private $id_main;

    private $barcode_propery = [
        'height' => 120,
        'fcolor' => "000000",
        'text_color' => "000000",
    ];

    private $data = [];
    private $type = "";

    public function create_ticket($id_main, $type) {
        $this->id_main = $id_main;
        $this->type = $type;
        switch ($type)
        {
            case "partner":

                $this->ticketPartner();

                break;
            case "guests":

                $this->ticketGuests();

                break;
        }
    }

    private function ticketPartner () {

        $mng_get_data = new GetData();
        $this->data = $data = $mng_get_data->getItemAllDataPartner($this->id_main);

        if (empty($this->data)) return false;

        $mng_html_pdf = new HtmlToPdf("A4");
        $mng_html_pdf->setTemplateFile("files/templates/ticket.html");
        $mng_html_pdf->setSaveDir("files/tickets/partner/");

        $itemTemplate = file_get_contents("files/templates/ticket_item.html");
        $ticketItemsResult = "";



        /* ITEM Відповідальний  */
        if ($data['main_info']['barcode_v'] == NULL OR $data['main_info']['barcode_v'] == "")
        {
            $barcode = $this->getBarCode($data['main_info']['id'], true);
        }else {
            $barcode = $data['main_info']['barcode_v'];
        }
        $replace = [
            'NameFactory' => $data['dogovyr_info']['name_factory'],
            'PIP' => $data['main_info']['pip_v'],
            'BarCodeURL' => 'https://generator.barcodetools.com/barcode.png?gen=0&data='.$barcode.'&bcolor=FFFFFF&fcolor=000000&tcolor=000000&fh=14&bred=0&w2n=2.5&xdim=2&w=&h=120&debug=1&btype=11&angle=0&quiet=1&balign=2&talign=0&guarg=1&text=1&tdown=1&stst=1&schk=0&cchk=1&ntxt=1&c128=0',
        ];
        $ticketItemsResult .=  str_replace(array_keys($replace), array_values($replace), $itemTemplate);



        /* ITEM Предствник  */
        if (!empty($data['guest_info']))
        {
            foreach ($data['guest_info'] as $item)
            {
                if ($item['barcode'] == NULL OR $item['barcode'] == "")
                {
                    $barcode = $this->getBarCode($item['id'], false);
                }else {
                    $barcode = $item['barcode'];
                }
                $replace = [
                    'NameFactory' => $data['dogovyr_info']['name_factory'],
                    'PIP' => $item['pip_guest'],
                    'BarCodeURL' => 'https://generator.barcodetools.com/barcode.png?gen=0&data='.$barcode.'&bcolor=FFFFFF&fcolor=000000&tcolor=000000&fh=14&bred=0&w2n=2.5&xdim=2&w=&h=120&debug=1&btype=11&angle=0&quiet=1&balign=2&talign=0&guarg=1&text=1&tdown=1&stst=1&schk=0&cchk=1&ntxt=1&c128=0',
                ];

                $ticketItemsResult .=  str_replace(array_keys($replace), array_values($replace), $itemTemplate);
            }
        }

        $mng_html_pdf->replaceAndCreate("ItemsResult", $ticketItemsResult, $this->id_main);
    }

    private function ticketGuests () {
        $mng_get_data = new GetData();
        $this->data = $data = $mng_get_data->getAllDataVisitorsById($this->id_main);

        if (empty($this->data)) return false;

        $mng_html_pdf = new HtmlToPdf("A4");
        $mng_html_pdf->setTemplateFile("files/templates/ticket.html");
        $mng_html_pdf->setSaveDir("files/tickets/guests/");

        $itemTemplate = file_get_contents("files/templates/ticket_item.html");
        $ticketItemsResult = "";


        /* ITEM Відповідальний  */
        if ($data['main_info']['barcode_v'] == 0 OR $data['main_info']['barcode_v'] == "" OR $data['main_info']['barcode_v'] == NULL)
        {
            $barcode = $this->getBarCode($data['main_info']['id'], true);
        }else {
            $barcode = $data['main_info']['barcode_v'];
        }
        $replace = [
            'NameFactory' => $data['main_info']['name_factory'],
            'PIP' => $data['main_info']['pip_v'],
            'BarCodeURL' => 'https://generator.barcodetools.com/barcode.png?gen=0&data='.$barcode.'&bcolor=FFFFFF&fcolor=000000&tcolor=000000&fh=14&bred=0&w2n=2.5&xdim=2&w=&h=120&debug=1&btype=11&angle=0&quiet=1&balign=2&talign=0&guarg=1&text=1&tdown=1&stst=1&schk=0&cchk=1&ntxt=1&c128=0',
        ];
        $ticketItemsResult .=  str_replace(array_keys($replace), array_values($replace), $itemTemplate);

        /* ITEM Предствник  */
        if (!empty($data['guest_info']))
        {
            foreach ($data['guest_info'] as $item)
            {
                if ($item['barcode'] == 0 OR $item['barcode'] == NULL OR $item['barcode'] == "")
                {
                    $barcode = $this->getBarCode($item['id'], false);
                }else {
                    $barcode = $item['barcode'];
                }
                $replace = [
                    'NameFactory' => $data['main_info']['name_factory'],
                    'PIP' => $item['pip_guest'],
                    'BarCodeURL' => 'https://generator.barcodetools.com/barcode.png?gen=0&data='.$barcode.'&bcolor=FFFFFF&fcolor=000000&tcolor=000000&fh=14&bred=0&w2n=2.5&xdim=2&w=&h=120&debug=1&btype=11&angle=0&quiet=1&balign=2&talign=0&guarg=1&text=1&tdown=1&stst=1&schk=0&cchk=1&ntxt=1&c128=0',
                ];

                $ticketItemsResult .=  str_replace(array_keys($replace), array_values($replace), $itemTemplate);
            }
        }

        $mng_html_pdf->replaceAndCreate("ItemsResult", $ticketItemsResult, $this->id_main);
    }

    /**
     * @param $id_guest
     * @param bool $isVidpovid
     * @return string
     */
    public function getBarCode($id_guest, $isVidpovid = false) {
        $result = "99";

        $savePropery = [];

        // індикатор учасник чи відвідувач
        switch ($this->type)
        {
            case "partner":
                $result .="1";
                $savePropery['field_name_id_main'] = 'id_partner_main';
                $savePropery['tableName'] = 'partner_';
                break;
            case "guests":
                $result .= "2";
                $savePropery['field_name_id_main'] = 'id_guest_main';
                $savePropery['tableName'] = 'guests_';
                break;
        }

        // id компанії
        for($i=0; $i<(3-strlen($this->data['main_info']['id'])) ;$i++)
            $result .="0";
        $result .= $this->data['main_info']['id'];

        // якщо не представник  ід+2 гостя якщо відповідальний 00001
        if (!$isVidpovid)
        {
            for($i=0; $i<(4-strlen($id_guest));$i++)
                $result .="0";
            $result .= $id_guest."2";
            $savePropery['tableName'] .= 'item';
            $savePropery['barcode_field_name'] = 'barcode';
        }else
        {
            $result .= "00001";
            $savePropery['tableName'] .= 'main';
            $savePropery['barcode_field_name'] = 'barcode_v';
        }
        $result .= rand(0, 9);

        //save
        $this->db->update([$savePropery['barcode_field_name'] => $result], $savePropery['tableName'], " `id` = $id_guest");


        return $result;
    }


}