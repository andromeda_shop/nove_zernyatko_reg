<?php

/**
 * Created by PhpStorm.
 * User: i.yuzkiv
 * Date: 28.02.2019
 * Time: 10:29
 */

namespace nz_admin\model;

use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use core\BaseModel;

class Mail extends BaseModel
{
    /**
     * @var Logger
     */
    private $log;
    /**
     * @var array
     */
    private $smtp_config;
    /**
     * @var array
     */
    private $mail_content = array(
        "subject" => "",
        "body" => "",
        "alt_body" => "",
    );
    /**
     * @var array
     */
    private $recipients = array();
    /**
     * @var PHPMailer
     */
    private $p_mail;
    /**
     * @var int
     */
    private $SMTPDebug = 0;
    /**
     * @var bool
     */
    private $SMTPAuth = true;

    /**
     * @var
     * Вкладення (файл)
     */
    private $attachments;

    /**
     * @var
     */
    private $attachments_name;

    /**
     * Mail constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->log = new Logger("mail");
        $this->log->pushHandler(new StreamHandler('nz_admin/admin_log/mail.log'));
        $this->smtp_config = $this->config->prepareJsonConfig($this->config->get("smtp_params"));
        $this->p_mail = new PHPMailer(true);
        $this->p_mail->CharSet = PHPMailer::CHARSET_UTF8;
    }

    public function sendMail() {
        try {
            $this->p_mail->SMTPDebug = $this->SMTPDebug;
            $this->p_mail->isSMTP();
            $this->p_mail->SMTPAuth = $this->SMTPAuth;
            $this->p_mail->Host = $this->smtp_config['host'];
            $this->p_mail->Username = $this->smtp_config['user_name'];
            $this->p_mail->Password = $this->smtp_config['user_password'];
            $this->p_mail->SMTPSecure = $this->smtp_config['smtp_secure'];
            $this->p_mail->Port = $this->smtp_config['port'];

            $this->p_mail->setFrom($this->smtp_config['from_mail'], $this->smtp_config['from_name']);

            foreach ($this->recipients as $item_r) {
                $this->p_mail->addAddress($item_r);
            }

            $this->p_mail->isHTML();
            $this->p_mail->Subject = $this->mail_content['subject'];
            $this->p_mail->Body = $this->mail_content['body'];
            $this->p_mail->AltBody = $this->mail_content['alt_body'];

            if ($this->attachments != "" OR $this->attachments != NULL)
            {
                if ($this->attachments_name != NULL OR $this->attachments_name != "")
                {
                    $this->p_mail->addAttachment($this->attachments, $this->attachments_name);
                }else {
                    $this->p_mail->addAttachment($this->attachments);
                }


            }


            $this->p_mail->send();
            $mail = (isset($this->recipients[1])) ? implode(" , " , $this->recipients) : $this->recipients[0];
            $this->writeLog("Повідомлення відправленно успішно: ".$mail);
            return true;

        }catch (Exception $e)
        {
            //dump($this->p_mail->ErrorInfo);
            $this->log->error("Не вдалось відправти повідомлення recipient: |".$this->p_mail->ErrorInfo);
        }

        return $this->smtp_config;
    }

    /**
     * @param $mail_content_subject
     * @return $this
     */
    public function setMailContentSubject($mail_content_subject)
    {
        $this->mail_content["subject"] = $mail_content_subject;
        return $this;
    }

    /**
     * @param $mail_content_body
     * @return $this
     */
    public function setMailContentBody($mail_content_body)
    {
        $this->mail_content["body"] = $mail_content_body;
        return $this;
    }

    /**
     * @param $mail_content_alt_body
     * @return $this
     */
    public function setMailContentAltBody($mail_content_alt_body)
    {
        $this->mail_content["alt_body"] = $mail_content_alt_body;
        return $this;
    }


    /**
     * @param $recipients
     * @return $this
     */
    public function setRecipients($recipients)
    {
        $this->recipients = $recipients;
        return $this;
    }

    public function setAttachments($attachments) {
        $this->attachments = $attachments;
        return $this;
    }

    public function setAttachmentsName($attachmentsName) {
        $this->attachments_name= $attachmentsName;
        return $this;
    }


    public function setSMTPDebug($SMTPDebug)
    {
        $this->SMTPDebug = $SMTPDebug;
        return $this;
    }

    public function writeLog($text = "") {
        $this->log->error($text." | ".$this->p_mail->ErrorInfo);
    }
}