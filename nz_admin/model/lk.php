<?php
/**
 * Created by PhpStorm.
 * User: i.yuzkiv
 * Date: 14.03.2019
 * Time: 9:52
 */

namespace nz_admin\model;


use core\BaseModel;
use nz_admin\core\BaseController;

class lk extends BaseController
{

    private $type;
    private $data;
    private $data_lk;
    private $id;
    private $password = "";

    public function open_lk ($id, $type) {
        $this->type = $type;
        $this->id = $id;

        switch ($type)
        {
            case "partner":
                $result = $this->open_lk_partner();
                break;
            case "guests":
                $result = $this->open_lk_guests();
                break;
        }
    }

    /**
     * @param bool $send_mail
     * @return bool|string
     */
    private function open_lk_partner($send_mail = true) {
        $mng_getData = new GetData();
        $data_lk = $mng_getData->getDataLkById($this->id, $this->type);
        $data = $this->data = $mng_getData->getItemAllDataPartner($this->id);

        $this->password=$this->genPasswordLk();
        if ($data_lk == NULL)
        {
            $data_lk['id_partner_main'] = $data['main_info']['id'];
            $data_lk['email'] = $data['main_info']['email_v'];
            $data_lk['mob'] = $data['main_info']['mob_v'];
            $data_lk['password'] = $this->password;
            $data_lk['status'] = 1;

            $this->data_lk = $data_lk;
            $result =  $this->db->insert_get_id($data_lk, "partner_lk");

        }else {
            $this->data_lk = $data_lk;
            $this->password = $data_lk['password'];
            $result = $data_lk['id'];
        }

        if ($send_mail)
        {
            $this->sendMailAccess();
        }

        return $result;

    }
    private function open_lk_guests($send_mail = true){
        $mng_getData = new GetData();
        $data_lk = $mng_getData->getDataLkById($this->id, $this->type);
        $data = $this->data = $mng_getData->getAllDataVisitorsById($this->id);

        $this->password=$this->genPasswordLk();

        if ($data_lk == NULL)
        {
            $data_lk['id_guest_main'] = $data['main_info']['id'];
            $data_lk['email'] = $data['main_info']['email_v'];
            $data_lk['mob'] = $data['main_info']['mob_v'];
            $data_lk['password'] = $this->password;
            $data_lk['status'] = 1;

            $this->data_lk = $data_lk;
            $result =  $this->db->insert_get_id($data_lk, "guests_lk");

        }else {
            $this->data_lk = $data_lk;
            $this->password = $data_lk['password'];
            $result = $data_lk['id'];
        }

        if ($send_mail)
        {
            $this->sendMailAccess();
        }

        return $result;
    }

    /**
     * @return bool
     */
    private function sendMailAccess()
    {
        $lk_info = "
        <br> <h2>Відкрито доступ в особистий кабінет.</h2>
        
        <table>
            <tr>
                <th style='border: 1px solid black;' >Логін:</th>
                <td style='border: 1px solid black;' >".$this->data_lk['email']." або ".$this->data_lk['mob']."</td>
            </tr>
            <tr>
                <th style='border: 1px solid black;' >Пароль:</th>
                <td style='border: 1px solid black;' >".$this->password."</td>
            </tr>
        </table>
        
        <h4><a href='https://registation.novezernyatko.com.ua/login'>Сторінка авторизації</a></h4>";

        $mail = new Mail();

        $mail->setRecipients([$this->data_lk['email']])
            ->setMailContentSubject("Доступ в особистий кабінет .Ⅳ-й Міжнародний Форум аграрних інновацій «Нове Зернятко»");

        $mail->setMailContentBody($lk_info." <h3>Дякуємо!</h3> </div>");
        $mail->setMailContentAltBody("Доступ в особистий кабінет. Логін: ".$this->data_lk['email']." або ".$this->data_lk['mob'].", Пароль: ".$this->password.". https://registation.novezernyatko.com.ua/login Дякуємо!");

        $mail->sendMail();

        return true;

    }

    /**
     * @param int $length
     * @return mixed
     */
    public function genPasswordLk($length = 10) {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789!@#$%^&*()";
        $pass = [];
        for ($i = 0; $i < $length; $i++) {
            $n = rand(0, strlen($alphabet)-1);
            $pass[$i] = $alphabet[$n];
        }
        return implode($pass);
    }

}