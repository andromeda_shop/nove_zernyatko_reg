<?php

/**
 * Created by PhpStorm.
 * User: i.yuzkiv
 * Date: 11.02.2019
 * Time: 16:53
 */

namespace nz_admin\model;

class GetData extends \core\BaseModel
{
    /**
     * @var string
     * File dogovir format
     */
    private $dogovyr_format = ".pdf";
    /**
     * @var string
     * File dogovir save dir
     */
    private $dogovyr_save_dir = "files/company_dogovyr/";
    /**
     * @var array
     * File logo format
     *
     */
    private $logo_format = array(".pdf", ".eps", ".cdr", ".ai",'.zip','.rar');
    /**
     * @var string
     * File logo save dir
     */
    private $logo_save_dir = "files/logo_company/";

    /**
     * @return array
     * Базова Інфомація про усіх учасників
     */
    public function getBaseDataPartners () {

        $base_data_company = $this->db->CreateQuery("SELECT 
                                                    `partner_main`.`id`, `partner_main`.`pip_v`, `partner_main`.`mob_v`, 
                                                    `partner_main`.`email_v`,  `partner_main`.`status`, 
                                                    `partner_main`.`data_registration`, `partner_main`.`id_package_services`,
                                                    `package_services`.`name` as 'taryf', `partner_main`.`diyalnist_id`,
                                                    `partner_dogovyr`.`name_factory`, `partner_dogovyr`.`pip_dyr`,
                                                    `partner_dogovyr`.`mob_number` as 'mob_number_dir'
                                                    FROM ((`partner_main` INNER JOIN `partner_dogovyr` 
                                                    ON `partner_main`.`id` = `partner_dogovyr`.`id_partner_main`) INNER JOIN `package_services` ON `partner_main`.`id_package_services` = `package_services`.id);
        ");

        return $base_data_company;

    }

    /**
     * @param $id
     * @return array
     */
    public function getItemAllDataPartner ($id) {
        
        $main_info = $this->db->setSelect("*")->setFrom("partner_main")->setWhere("id=".$id)->one();

        $diyalnist =  array();
        foreach (explode(",", $main_info['diyalnist_id']) as $item)
        {
            $diyalnist[$item] =  $this->db->setSelect("name")->setFrom("diyalnist_item")->setWhere("id=".$item)->one()['name'];
        }
        $main_info['diyalnist'] = $diyalnist;
        $main_info['diyalnist_implode'] = implode(array_values($diyalnist), " , ");

        $main_info['package_services'] = $this->db->setSelect("*")->setFrom("package_services")->setWhere("id=".$main_info['id_package_services'])->one();

        $dogovyr_info = $this->db->setSelect("*")->setFrom("partner_dogovyr")->setWhere("id_partner_main=".$id)->one();
        $guest_info = $this->db->setSelect("*")->setFrom("partner_item")->setWhere("id_partner_main=".$id)->all();

        $main_info['len_reg_guest'] = count($guest_info) + 1;

        $dogovyr_info['name_factory'] = str_replace(['"', "{", "}", '&'], '', $dogovyr_info['name_factory']);
        
        return array(
            'main_info' => $main_info,
            'dogovyr_info' => $dogovyr_info,
            'guest_info' => $guest_info
        );
        
    }

    /**
     * @param $id_partner
     * @return array
     * Інфомрація для договру
     */
    public function getDataItemDogovyr($id_partner) {
        $dogovyr_info = $this->db
            ->setSelect("*")
            ->setFrom("partner_dogovyr")
            ->setWhere("id_partner_main = " . $id_partner)
            ->one();

        $dyr_short = explode(" ", $dogovyr_info['pip_dyr']);
        $dyr_short[1] = mb_strtoupper(mb_substr($dyr_short[1], 0, 1)) . ". ";
        $dyr_short[2] = mb_strtoupper(mb_substr($dyr_short[2], 0, 1)) . ".";
        $dyr_short = implode(" ", $dyr_short);

        $dogovyr_info['dyr_short'] = $dyr_short;

        $base_data = $this->db->setSelect("*")->setFrom("partner_main")->setWhere("id=".$id_partner)->one();

        $dogovyr_info['package_services_info'] = $this->db->setFrom('package_services')->setWhere('id='.$base_data['id_package_services'])->one();

        return $dogovyr_info;
    }

    /**
     * @param $id
     * @param string $type
     * @return array
     * Посилання на файли для завантаження
     */
    public function GetPartnerFileInfo($id, $type = 'partner'){

        $dogovyr = (file_exists($this->dogovyr_save_dir.$id.$this->dogovyr_format)) ? $this->dogovyr_save_dir.$id.$this->dogovyr_format : "";

        $spec = (file_exists($this->dogovyr_save_dir."cert/".$id.$this->dogovyr_format)) ? $this->dogovyr_save_dir."cert/".$id.$this->dogovyr_format : "";


        $logo = "";
        foreach ($this->logo_format as $item)
        {
            if (file_exists($this->logo_save_dir.$id.$item))
            {
                $logo = $this->logo_save_dir.$id.$item;
                break;
            }
        }
        $ticket = file_exists("files/tickets/$type/$id.pdf") ? "files/tickets/$type/$id.pdf" : "";
        return array(
            "dogovyr" => $dogovyr,
            "spec" => $spec,
            "logo" => $logo,
            "ticket" => $ticket
        );
    }

    /**
     * @return array
     * Інформація про відвідувачів для таблиція
     */
    public function getBaseDataVisitors () {
        return $this->db
            ->setSelect("`id`, `name_factory`, `pip_v`, `posada_v`, `mob_v`, `email_v`, `status`, `data_registration`")
            ->setFrom("guests_main")
            ->setOrderBy("name_factory ASC")
            ->all();
    }

    public function getAllDataVisitorsById ($id) {

        $main_info = $this->db
            ->setSelect("*")
            ->setFrom("guests_main")
            ->setWhere("id=".$id)
            ->one();

        $diyalnist =  array();
        foreach (explode(",", $main_info['diyalnist_id']) as $item)
        {
            $diyalnist[$item] =  $this->db->setSelect("name")->setFrom("diyalnist_item")->setWhere("id=".$item)->one()['name'];
        }
        $main_info['diyalnist'] = $diyalnist;
        $main_info['diyalnist_implode'] = implode(array_values($diyalnist), " , ");

        $guest_info = $this->db
            ->setSelect("*")
            ->setFrom("guests_item")
            ->setWhere("id_guest_main=".$id)
            ->all();

        $main_info['len_reg_guest'] = count($guest_info);

        return [
            "main_info" => $main_info,
            "guest_info" => $guest_info
        ];
    }

    /**
     * @param $id
     * @param $type
     * @return array
     */
    public function getDataLkById($id, $type) {
        $id_main_name = ($type == 'guests') ? 'id_guest_main' : 'id_partner_main';
        return $this->db->setSelect("*")
            ->setFrom($type."_lk")
            ->setWhere($id_main_name."=".$id)
            ->one();
        /*return $this->db->sql;*/
    }

}