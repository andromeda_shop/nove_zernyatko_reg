<?php
/**
 * Created by PhpStorm.
 * User: i.yuzkiv
 * Date: 01.03.2019
 * Time: 15:20
 */

namespace nz_admin\model;


use core\BaseModel;
use Psr\Log\Test\DummyTest;

class UpdateRegData extends BaseModel
{

    /**
     * @var string
     * partners or guests
     */
    private $type_update = "";
    /**
     * @var int
     */
    private $id_main;
    /**
     * @var array
     * _POST
     */
    private $data_update;
    /**
     * @var array
     * Дані для валідації в талицях
     */
    private $table_check = [
        'guests_main' => [
            'email' => 'email_v',
            'mob' => 'mob_v',
            "id_main" => 'id'
        ],
        'guests_item' => [
            'email' => 'email_guest',
            'mob' => 'mob_num_guest',
            "id_main" => 'id_guest_main'
        ],
        'partner_main' => [
            'email' => 'email_v',
            'mob' => 'mob_v',
            "id_main" => 'id'
        ],
        'partner_item' => [
            'email' => 'email_guest',
            'mob' => 'mob_num_guest',
            "id_main" => 'id_partner_main'
        ],
    ];
    /**
     * @var array
     */
    private $errors = [];

    /**
     * @param $id
     * @param $data
     * @param $type_update
     * @return mixed
     */
    public function updatePartnerData($id, $data, $type_update="partner")
    {
        $this->id_main = $id;
        $this->type_update = $type_update;
        $this->data_update = $data;

        if ($this->validDogovyr() AND $this->validPhone() AND $this->validEmails() AND $this->validaEmailInArray() AND $this->validaMobInArray()) {

            $this->data_update['base_info'] = $this->comparePostFieldAndDb($this->data_update['base_info'], "partner_main");
            $this->db->update($this->data_update['base_info'], "partner_main", "id=".$this->id_main);

            if (!empty($this->data_update['guset_info'])) {
                foreach ($this->data_update['guset_info'] as $item){
                    $item = $this->comparePostFieldAndDb($item, "partner_item");

                    if ($item['id'] == 'insert') {

                        $item['id_partner_main'] = $this->id_main;
                        unset($item['id']);
                        $this->db->insert($item, 'partner_item');

                    }else {
                        $this->db->update($item, "partner_item", "id=".$item['id']."  AND id_partner_main = ".$this->id_main);
                    }
                }
            }

            /*delete guests*/
            if (!empty($this->data_update['remove_guest_id']) or !empty($this->data_update['remove_guest_id']) != "")
            {
                foreach ($this->data_update['remove_guest_id'] as $item_del_id) {
                    $this->db->CreateQuery("DELETE FROM `partner_item` WHERE `id`=".$item_del_id);
                }
            }


            $this->data_update['dogovyr_info'] = $this->comparePostFieldAndDb($this->data_update['dogovyr_info'], "partner_dogovyr");

            // провідміняти посаду директора
            $this->data_update['dogovyr_info']['posada_dyr_rod'] = (new NameCase())->providWord($this->data_update['dogovyr_info']['posada_dyr'], 1);

            //dump()

            $this->db->update($this->data_update['dogovyr_info'], "partner_dogovyr", " id_partner_main = ".$this->id_main);

            return json_encode(array('true'));
        }else {
            $errors = array();
            foreach ($this->errors as $item)
            {
                $errors[] = $this->config->get($item);
            }
            return json_encode($errors);
        }
    }

    public function updateGuestsData($id, $data, $type_update="guests")
    {
        $this->id_main = $id;
        $this->type_update = $type_update;
        $this->data_update = $data;

        if ($this->validEmails() AND $this->validPhone() AND $this->validaEmailInArray() AND $this->validaMobInArray()) {

            $this->data_update['base_info'] = $this->comparePostFieldAndDb($this->data_update['base_info'], "guests_main");

            $this->db->update($this->data_update['base_info'], "guests_main", "id=".$this->id_main);
            if (!empty($this->data_update['guset_info'])) {
                foreach ($this->data_update['guset_info'] as $item) {
                    $item = $this->comparePostFieldAndDb($item, "guests_item");
                    if ($item['id'] == 'insert') {
                        $item['id_guest_main'] = $this->id_main;
                        $this->db->insert($item, 'guests_item');
                    } else {
                        $this->db->update($item, "guests_item", "id=" . $item['id'] . "  AND id_guest_main = " . $this->id_main);

                    }
                }
            }
            return json_encode(array('true'));
        }else {
            $errors = array();
            foreach ($this->errors as $item)
            {
                $errors[] = $this->config->get($item);
            }
            return json_encode($errors);
        }


    }

    /**
     * @return bool
     * Перевірка всіх поштових адрес окрім
     */
    public function validEmails() {
        $where = "";

        if (isset($this->data_update['guset_info'])) {
            foreach ($this->data_update['guset_info'] as $item) {
                $where .= " %email% LIKE '".$item['email_guest']."' OR ";
            }
        }

        $where = $where." %email% LIKE '".$this->data_update['base_info']['email_v']."' ";

        foreach ($this->table_check as $key => $item)
        {
            $check = null;
            $where_item = str_replace("%email%", $item['email'], $where);

            if (preg_match('/'.$this->type_update.'/', $key))
            {
                $check = $this->db->CreateQuery("SELECT * FROM ".$key." WHERE (".$where_item.") AND ".$item['id_main']."<>".$this->id_main);
            }else {
                $check = $this->db->CreateQuery("SELECT * FROM ".$key."  WHERE (".$where_item.")");
            }

            if ($check != NULL) {
                $this->errors[] = "nz_admin_update_items_dubl_email";
                break;
            }
        }
        return (!empty($check)) ? false : true;
    }
    /**
     * @return bool
     * Перевірка всіх мобільних номерів окрім
     */
    public function validPhone() {
        $where = "";
        if (isset($this->data_update['guset_info'])) {
            foreach ($this->data_update['guset_info'] as $item) {
                $where .= " %mob% LIKE '".$item['mob_num_guest']."' OR ";
            }
        }
        $where = $where." %mob% LIKE '".$this->data_update['base_info']['mob_v']."' ";

        foreach ($this->table_check as $key => $item)
        {
            $check = null;
            $where_item = str_replace("%mob%", $item['mob'], $where);

            if (preg_match('/'.$this->type_update.'/', $key))
            {
                $check = $this->db->CreateQuery("SELECT * FROM ".$key." WHERE (".$where_item.") AND ".$item['id_main']."<>".$this->id_main);
            }else {
                $check = $this->db->CreateQuery("SELECT * FROM ".$key."  WHERE (".$where_item.")");
            }

            if ($check != NULL) {
                $this->errors[] = "nz_admin_update_items_dubl_mob";
                break;
            }
        }
        return (!empty($check)) ? false : true;
    }

    /**
     * @return bool
     * Перевірка реквізитиів договору
     */
    public function validDogovyr() {
        $post_data = $this->data_update['dogovyr_info'];

        $check = $this->db
            ->CreateQuery("SELECT `id` FROM `partner_dogovyr` 
            WHERE (`edrpov` = ".$post_data['edrpov']." OR `ipn` = ".$post_data['ipn']." OR `roz_rah` = ".$post_data['roz_rah']." OR `name_factory` LIKE '".$post_data['name_factory']."') 
            AND id_partner_main <> ".$this->id_main.";");

        $check1 = $this->db
            ->CreateQuery("SELECT `id` FROM `guests_main` WHERE `name_factory` LIKE '".$post_data['name_factory']."';");

        if (!empty($check) OR !empty($check1)) {
            $this->errors[] = "nz_admin_update_items_dubl_dogovyr";
            return false;
        }else return true;
    }

    /**
     * @return bool
     * Перевірка поштових адрес в масві
     */
    public function validaEmailInArray ()
    {
        $emails = array();
        $error = false;
        if (!empty($this->data_update['guset_info']))
        {
            foreach ($this->data_update['guset_info'] as $item)
            {
                if (!in_array($item['email_guest'], $emails))
                {
                    $emails[] = $item['email_guest'];
                }else {
                    $error = true;
                    break;
                }
            }
        }
        if (!$error AND !in_array($this->data_update['base_info']['email_v'], $emails)) {
            return true;
        }else {
            $this->errors[] = "nz_admin_update_items_dubl_email";
            return false;
        }

    }
    /**
     * @return bool
     * Перевірка номерів телефонів в масиві
     */
    public function validaMobInArray() {

        $mob = array();
        $error = false;
        
        if (!empty($this->data_update['guset_info']))
        {
            foreach ($this->data_update['guset_info'] as $item)
            {
                if (!in_array($item['mob_num_guest'], $mob))
                {
                    $mob[] = $item['mob_num_guest'];
                }else {
                    $error = true;
                    break;
                }
            }
        }

        if (!$error AND !in_array($this->data_update['base_info']['mob_v'], $mob)) {
            return true;
        }else {
            $this->errors[] = "nz_admin_update_items_dubl_mob";
            return false;
        }
    }

    /**
     * @param $id_main
     * @param string $typ
     * Зберегти дамп орігінальної інфомрації інформації
     */
    public function saveBackOriginalData ($id_main, $typ) {
        $result = [];

        switch ($typ)
        {
            case "partner":
                $result['base_info'] = $this->db->setSelect("*")->setFrom("partner_main")->setWhere("id=".$id_main)->one();
                $result['guest_info'] = $this->db->setSelect("*")->setFrom("partner_item")->setWhere("id_partner_main=".$id_main)->all();
                $result['dogovir_info'] = $this->db->setSelect("*")->setFrom("partner_dogovyr")->setWhere("id_partner_main=".$id_main)->all();
                break;
            case "guests":
                $result['base_info'] = $this->db->setSelect("*")->setFrom("guests_main")->setWhere("id=".$id_main)->one();
                $result['guest_info'] = $this->db->setSelect("*")->setFrom("guests_item")->setWhere("id_partner_main=".$id_main)->all();
                break;
        }


        $result = json_encode($result, JSON_UNESCAPED_UNICODE);
        $file_name = "files/original_data/$typ/$id_main.txt";
        if (!file_exists($file_name))
        {
            file_put_contents($file_name, $result);
        }
    }

    /**
     * @param array $_post_data Тільки одновимірний масив (вкладені масив ні, перебирати циклом)
     * @param string $table_name
     * @return array
     * Видаляю лишні поля з масиву, робилось для Internet Explorer
     * Бо він додавав в POST Лишні поля
     */
    public function comparePostFieldAndDb($_post_data = array(), $table_name = '') {

        $db_data = $this->db->CreateQuery("DESCRIBE `$table_name`");
        $db_field = array();
        foreach ($db_data as $itemDbData) {
            $db_field[] = $itemDbData['Field'];
        }

        foreach ($_post_data as $key => $item)
        {
            if (!in_array($key, $db_field))
            {
                unset($_post_data[$key]);
            }else continue;
        }

        return $_post_data;
    }

}