<?php
/**
 * Created by PhpStorm.
 * User: i.yuzkiv
 * Date: 27.02.2019
 * Time: 12:50
 */

namespace nz_admin\model;


use core\BaseModel;

class TableFilter extends BaseModel
{

    private $table_data = array();
    private $hide_rows = array('id');
    private $rows_caption = array();
    private $href_template = "";
    private $href_id_filed_name = "id";
    private $auto_increment = false;
    private $sort_type = "SORT_DESC";
    private $sort_field = "id";

    public function createTable() {

        $tableConfig = [];
        $auto_increment = 0;

        $this->table_data = array_orderby($this->table_data, $this->sort_field, constant($this->sort_type));

        foreach ($this->table_data as $key_tr => $item_tr)
        {
            $auto_increment++;

            foreach ($item_tr as $key_td => $item_td)
            {
                if ((!in_array($key_td,$this->hide_rows)))
                {
                    $tableConfig[$key_tr][$key_td] = $item_td;
                }
                if ($this->auto_increment == true)
                {
                    $tableConfig[$key_tr]["increment"] = $auto_increment;
                }

            }
            if ($this->href_template != "")
            {
                $tableConfig[$key_tr]['data']['data-href'] = $this->href_template.$item_tr[$this->href_id_filed_name];
            }
        }

        $result = $this->setConfig();
        $result["show_data"] = $tableConfig;
        return $result;
    }


    private function setConfig () {

        $row_caption = array();
        foreach ($this->rows_caption as $key => $item) {
            if (!in_array($key, $this->hide_rows))
            {
                $row_caption[$key] = $item;
            }
        }

        return [
            "base_config" => [
                "rows_caption" => $row_caption,
                "auto_increment" => $this->auto_increment,
                "sort_type" => $this->sort_type,
                "sort_filed" => $this->sort_field

            ],
            "show_data" => [],
        ];
    }

    /**
     * @param $table_data
     * @return $this
     */
    public function setTableData($table_data)
    {
        $this->table_data = $table_data;
        return $this;
    }

    /**
     * @param $hide_rows
     * @return $this
     */
    public function setHideRows($hide_rows)
    {
        $this->hide_rows = $hide_rows;
        return $this;
    }

    /**
     * @param $rows_caption
     * @return $this
     */
    public function setRowsCaption($rows_caption)
    {
        $this->rows_caption = $rows_caption;
        return $this;
    }

    /**
     * @param $href_template
     * @return $this
     */
    public function setHrefTemplate($href_template)
    {
        $this->href_template = $href_template;
        return $this;
    }

    /**
     * @param $auto_increment
     * @return $this
     */
    public function setAutoIncrement($auto_increment)
    {
        $this->auto_increment = $auto_increment;
        return $this;
    }

    /**
     * @param $sort_field
     * @return $this
     */
    public function setSortField($sort_field)
    {
        $this->sort_field = $sort_field;
        return $this;
    }

    /**
     * @param $sort_type
     * @return $this
     */
    public function setSortType($sort_type)
    {
        $this->sort_type = $sort_type;
        return $this;
    }
}