<?php

namespace  nz_admin\model;
use core\BaseModel;
use Mpdf\Mpdf;

/**
 * Created by PhpStorm.
 * User: i.yuzkiv
 * Date: 20.02.2019
 * Time: 11:21
 */
class HtmlToPdf extends BaseModel
{

    /**
     * @var string
     * file template
     */
    private $templateFile = "files/templates/html_dog/dog_1.html";
    /**
     * @var string
     * patch to save
     */
    private $saveDir = "files/company_dogovyr/";
    /**
     * @var string
     * template value
     */
    private $templateStr = "";
    /**
     * @var Mpdf 
     */
    private $Mpdf;

    public function __construct($format = "A4")
    {
        parent::__construct();

        $this->Mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => $format]);
    }


    /**
     * @param $search
     * @param $replace
     * @param String $file_name
     * @return bool
     * @throws \Mpdf\MpdfException
     */
    public function replaceAndCreate ($search, $replace, String $file_name)
    {
        $this->templateStr = file_get_contents($this->templateFile);

        $file_content = str_replace($search, $replace, $this->templateStr);
        $this->Mpdf->WriteHTML($file_content);
        $this->Mpdf->Output($this->saveDir.$file_name.".pdf");

        if (file_exists($this->saveDir.$file_name.".pdf"))
        {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * @param array $search
     * @param array $replace
     * @param String $file_name
     * @param array $templates
     * @throws \Mpdf\MpdfException
     * Декілька шаблонів в один файл (з нової сторінки)
     */
    public function replaceAndCreateManyTemplates (Array $search, Array $replace, String $file_name, Array $templates) {

        foreach ($templates as $key => $item) {
            $file_content = str_replace($search, $replace, file_get_contents($item));
            $this->Mpdf->WriteHTML($file_content);

            if ( (count($templates) - 1) != $key)
            $this->Mpdf->AddPage();
        }
        $this->Mpdf->Output($this->saveDir.$file_name.".pdf");
    }

    /**
     * @param $value
     * @return null
     */
    public function encodeToWindows1251 ($value) {

        $result = null;
        if (is_string($value))
        {
            $result = mb_convert_encoding($value,"windows-1251", "utf-8");
        }else {
            foreach ($value as $key => $item) {
                if (is_array($item)){
                    $result[$key] = $item;
                }else {
                    $result[$key] = mb_convert_encoding($item,"windows-1251", "utf-8");
                }
            }
        }
        return $result;
    }

    /**
     * @param $templateFile
     */
    public function setTemplateFile($templateFile)
    {
        $this->templateFile = $templateFile;
    }

    /**
     * @param $saveDir
     */
    public function setSaveDir($saveDir)
    {
        $this->saveDir = $saveDir;
    }


}