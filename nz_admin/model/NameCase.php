<?php
/**
 * Created by PhpStorm.
 * User: i.yuzkiv
 * Date: 05.03.2019
 * Time: 11:03
 */

namespace nz_admin\model;


use core\BaseModel;

class NameCase extends BaseModel
{
    /**
     * @var \NCLNameCaseUa
     */
    private $nameCase;

    public function __construct()
    {
        parent::__construct();

        require_once "vendor/namecaselib/Library/NCLNameCaseUa.php";

        /*
        * 0 - Назвиний
        * 1 - Родович
        * 2 - Давальний
        * ...
        * */

        $this->nameCase = new \NCLNameCaseUa();
    }

    /**
     * @param $id_partner_main
     * @param bool $if_null
     * @param bool $save
     * @return bool|mixed
     */
    public function pipBossPartner($id_partner_main, $save = false, $if_null = false, $vidminok = 1) {
        //$all = $this->nameCase->q("Юзьків Ігор Володимирович");
        //dump($all);

        $where = "id_partner_main=".$id_partner_main;
        $where .= ($if_null) ? " AND pip_dyr_vid = NULL" : " ";


        $data = $this->db
            ->setSelect("pip_dyr")
            ->setFrom("partner_dogovyr")
            ->setWhere($where)
            ->one();

        if ($data == NULL)
        {
            return false;
        }
        else
        {
            $pip_dyr_vid = $this->nameCase->q($data['pip_dyr']);

            if ($save) {
                if ($this->db->update(['pip_dyr_vid' => $pip_dyr_vid[$vidminok]], "partner_dogovyr", "id_partner_main=".$id_partner_main)) {
                    return true;
                }else
                    return false;

            }else {
                return $pip_dyr_vid[$vidminok];
            }
        }

    }

    public function posadaBossPartner ($id_partner_main, $save = false, $if_null = false, $vidminok = 1) {
        $where = "`id_partner_main`=".$id_partner_main;
        $where .= ($if_null) ? " AND `posada_dyr` = NULL" : " ";


        $data = $this->db
            ->setSelect("posada_dyr")
            ->setFrom("partner_dogovyr")
            ->setWhere($where)
            ->one();

        if ($data == NULL)
        {
            return false;
        }
        else
        {
            $pip_dyr_vid = $this->nameCase->q($data['posada_dyr']);

            if ($save) {
                if ($this->db->update(['posada_dyr_rod' => $pip_dyr_vid[$vidminok]], "partner_dogovyr", "id_partner_main=".$id_partner_main)) {
                    return true;
                }else
                    return false;

            }else {
                return $pip_dyr_vid[$vidminok];
            }
        }
    }

    /**
     * @param string $word
     * @param int $vidminok
     * @return string
     */
    public function providWord ($word = "",  $vidminok = 1) {
        return ($word != "") ? $this->nameCase->q($word)[$vidminok] : "";
    }


}