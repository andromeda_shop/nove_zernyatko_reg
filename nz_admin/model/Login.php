<?php
/**
 * Created by PhpStorm.
 * User: i.yuzkiv
 * Date: 13.02.2019
 * Time: 10:56
 */

namespace nz_admin\model;


use core\BaseModel;
use nz_admin\core\BaseController;

class Login extends BaseModel
{

    public $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

    /**
     * @param $login
     * @param $password
     * @return array|bool
     */
    public function getUserByLoginPass ($login, $password)
    {
        $login = sha1($login);
        $password = sha1($password);

        $user = $this->db
            ->setSelect("id, token, status")
            ->setFrom("admin_user")
            ->setWhere("`login`='".$login."' AND `password`='".$password."'")
            ->one();

        return $user;
    }

    public function getUserById ($id)
    {

        $user = $this->db
            ->setSelect("id, token, status")
            ->setFrom("admin_user")
            ->setWhere("`id` = $id")
            ->one();

        return $user;
    }

    public function LoginUser ($login, $password) {

        if ($login != NULL AND $password != NULL)
        {
            $check_db = $this->getUserByLoginPass($login, $password);

            if ($check_db)
            {
                $token = $this->generateToken(false);
                if ($this->db->execute("UPDATE `admin_user` SET `token`='".$token."',`ip`='"._USER_IP."' WHERE `id` = ".$check_db['id']))
                {
                    $_SESSION['admin_user']['token'] = $token;
                    $_SESSION['admin_user']['user_id'] = $check_db['id'];
                    return true;
                }else return false;


            }else return false;
        } return false;

    }

    /**
     * @param bool $onlyNumber
     * @param int $cnt
     * @param bool $sha1
     * @return string
     */
    public function generateToken($onlyNumber = true, $sha1 = true, $cnt = 20) {
        $token = "";
        if ($onlyNumber)
        {
            for ($i=0; $i<$cnt; $i++)
            {
                $token .= rand(0, 9);
            }

        }else {
            $charsetLen = strlen($this->characters);
            for ($i = 0; $i < $cnt; $i++) {
                $token .= $this->characters[rand(0, $charsetLen - 1)];
            }
        }

        if ($sha1) $token = sha1($token);

        return $token;
    }

}