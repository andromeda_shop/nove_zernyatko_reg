<?php



/**
 * Created by PhpStorm.
 * User: i.yuzkiv
 * Date: 11.02.2019
 * Time: 16:44
 */
class PartnerController extends \nz_admin\core\BaseController
{
    /**
     * All partners
     */
    public function action_Partners()
    {

            $mng_get_data = new \nz_admin\model\GetData();
            $data = $mng_get_data->getBaseDataPartners();

            $TableFilter = new \nz_admin\model\TableFilter();
            $TableFilter
                ->setTableData($data)
                ->setHideRows(["id", "posada_v"])
                ->setHrefTemplate("/nz_admin/partners/item/")
                ->setAutoIncrement(true)
                ->setSortField("data_registration")
                ->setSortType("SORT_DESC")
                ->setRowsCaption([
                    "name_factory" => "Назва компанії",
                    "pip_v" => "ПІП упв.",
                    "posada_v" => "Посда упв.",
                    "mob_v" => "Моб. упв.",
                    "email_v" => "Email упв.",
                    "taryf" => "Тарифний план",
                    "status" => "Статус",
                    "data_registration" => "Дата"
                ]);

             $tableConfig = $TableFilter->createTable();
            $this->View
                ->setTitle("Учасники форуму")
                ->setH1Title("Учасники форуму")
                ->setUserScripts([
                    "/view/_system/js/jquery.maskedinput.js"
                ])
                ->render("partners/partner_table",
                    [
                        'tableConfig' => $tableConfig,
                        'base_data_company' => $data
                    ]
                );
        }

    /**
     * @param $id_partner
     * Edit item
     */
    public function action_item($id_partner)
    {
        $mng_get_data = new \nz_admin\model\GetData();
        $data = $mng_get_data->getItemAllDataPartner($id_partner);
        
        $this->View
            ->setTitle("Учасник ".$data['dogovyr_info']['name_factory'])
            ->setH1Title("Інфомрація про учасника форуму ".$data['dogovyr_info']['name_factory']." ( id = ".$data['main_info']['id'].")")
            ->setUserScripts([
                "/view/_system/js/jquery.maskedinput.js"
            ])
            ->setUserStyles([
                '/view/_system/css/loading.css'
            ])
            ->render("partners/partner_item",
                [
                    "data" => $data,
                    'file_info' => $mng_get_data->GetPartnerFileInfo($id_partner),
                    'param_pids_diy_dyr' => explode(";", $this->config->get("param_pids_diy_dyr")),
                ]
            );
    }

    /**
     * Створити договір
     */
    public function action_create_dogovyr()
    {
        if (isset($_POST['id']))
        {
            $mng_dog = new \nz_admin\model\CreateDogovyr();
            if ($genDog = $mng_dog->generateDogovyr($_POST['id']) == true){
                echo json_encode(array("true")); exit;
            }else {
                echo json_encode([$this->config->get("msg_creatre_dog_no_prepare")]); exit;
            }

        }else {
            echo json_encode(array("Ідентифікатор не передано!")); exit;
        }
    }

    /**
     * Download file
     */
    public function action_download_file ()
    {
        if (isset($_POST['url']))
        {
            if (file_exists($_POST['url'])) {

                $base_name = basename($_POST['url']);
                $file_name = ($_POST['file_name'] != "") ? $_POST['file_name'].substr($base_name, strpos($base_name,'.',0)) : $base_name;

                /*header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="'.$file_name.'"');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($_POST['url']));
                readfile($_POST['url']);*/

                header('Pragma: public');
                header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the Past
                header('Expires: 0');
                header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                header('Cache-Control: private', false);
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="'.$file_name.'"');
                readfile($_POST['url']);

                exit;
            }
        }
    }

    /**
     * @param $id
     * Update item
     */
    public function action_update($id)
    {
        $mng_update = new \nz_admin\model\UpdateRegData();
        $mng_update->saveBackOriginalData($id, "partner");
        echo $mng_update->updatePartnerData($id, $_POST, "partner");
        exit();
    }

    /**
     * @param $id
     * Провідмінтия ПІП Директора
     */
    public function action_name_case_item ($id)
    {
        $mng_name_case = new \nz_admin\model\NameCase();
        if ($mng_name_case->pipBossPartner($id, true) == true)
        {
            echo json_encode(['true']); exit;
        }else echo json_encode(['Упс, виникла внутрішня помилка, спробуйте пізніше']);
    }

    /**
     * @param $id
     */
    public function action_organiza_prav_forma ($id)
    {
        $mng_create_dogovyr = new \nz_admin\model\CreateDogovyr();
        $result = $mng_create_dogovyr->set_organiza_prav_forma($id);
        echo json_encode([$result]);
        exit();
    }

    /**
     * @param $id
     */
    public function action_prepare_data_dogovyr($id)
    {
        $mng_name_case = new \nz_admin\model\NameCase();
        $mng_create_dogovyr = new \nz_admin\model\CreateDogovyr();
        $result = [];

        if ($mng_name_case->pipBossPartner($id, true) === true)
            $result[] = "ПІП Директора в родовому відмінку - виконано";
        else
            $result[] = "ПІП Директора в родовому відмінку - не виконано";


        if ($mng_name_case->posadaBossPartner($id, true) === true)
            $result[] = "Посада Директора в родовому відмінку - виконано";
        else
            $result[] = "Посада Директора в родовому відмінку - не виконано";

        $pip_boss = $mng_create_dogovyr->set_organiza_prav_forma($id);
        if ($pip_boss === true)
        {
            $result[] = "Розшифрування абрівіатури організаційно правової форми - виконано";
        }else {
            $result[] = $pip_boss;
        }

        echo json_encode($result); exit();
    }


    public function action_create_ticket ($id)
    {

        $mng_c_ticket = new \nz_admin\model\CreateTicket();

        echo json_encode([$mng_c_ticket->create_ticket($id, "partner")]); exit();

    }

    public function action_open_lk($id)
    {
        echo json_encode([(new \nz_admin\model\lk())->open_lk($id, "partner")]); exit;
    }

    /**
     * Відпавити договір
     */
    public function action_send_dogovyr ()
    {

        if (empty($_POST) or !isset($_POST['id'])) $this->redirectToUrl("/nz_admin/partners/");
        $partner_info = (new \nz_admin\model\GetData()) -> getItemAllDataPartner($_POST['id']);

        $dogovyr_patch = "files/company_dogovyr/".$_POST['id'].".pdf";


        /*КОСТИЛІ*/
        if ($partner_info['main_info']['status'] != 3 OR !file_exists($dogovyr_patch))
        {
            echo json_encode(['Договір не сформовано, сформуйте договір та спрбуйте ще раз!']);
            exit;
        }
        if($partner_info['dogovyr_info']['posada_dyr_rod'] == '') {
            echo json_encode(['Заповніть поле "Посда директора", та сформуйте договір по новій! ']); exit;
        }
        if($partner_info['dogovyr_info']['org_pravova_forma'] == '') {
            echo json_encode(['Заповніть поле "Організаційно правова форма", та сформуйте договір по новій!']); exit;
        }
        /*КОСТИЛІ*/


        $html_text = '<p style="text-align: center;"><strong><span style="font-size: 16.0pt; line-height: 115%; font-family: \'Times New Roman\',serif;">Шановні учасники &laquo;ІV Міжнародного форуму аграрних інновацій&nbsp; &laquo;Нове зернятко&raquo; - Настасів 2019&raquo;!</span></strong></p>
                    <p style="text-align: justify; text-indent: 35.4pt;"><em><span style="font-size: 16.0pt; line-height: 115%; font-family: \'Times New Roman\',serif;">Щиро вдячні за те, що Ваша компанія виявила бажання прийняти участь у Форумі, який відбудеться 20-21 червня 2019 р.</span></em></p>
                    <p style="text-align: justify; text-indent: 35.4pt;"><em><span style="font-size: 16.0pt; line-height: 115%; font-family: \'Times New Roman\',serif;">Для підтвердження співпраці по участі та оперативної роботи по організації даного заходу, просимо обов&rsquo;язково роздрукувати два примірники договору (додається у вкладенні), підписані керівником компанії та вислати на поштову адресу:</span></em></p>
                    <p style="margin-bottom: .0001pt; text-align: justify; text-indent: 35.4pt; line-height: normal;"><em><span style="font-size: 16.0pt; font-family: \'Times New Roman\',serif;">ПАП &laquo;Агропродсервіс&raquo;, с. Настасів, Тернопільський р-н, Тернопільська обл., 47734</span></em></p>
                    
                    <p style="margin-bottom: .0001pt; text-align: justify; text-indent: 35.4pt; line-height: normal;"><em><span style="font-size: 16.0pt; font-family: \'Times New Roman\',serif;">Після одержання підписаних примірників договору на Вашу поштову адресу буде надіслано: ваш примірник договору, специфікацію, рахунок.</span></em></p>
                    
                    <p style="margin-bottom: .0001pt; text-align: justify; line-height: normal;"><em><span style="font-size: 16.0pt; font-family: \'Times New Roman\',serif;">&nbsp;</span></em></p>
                    <p><strong><em><span style="font-size: 16.0pt; line-height: 115%; font-family: \'Times New Roman\',serif;">З повагою оргкомітет Форуму!</span></em></strong></p>';


        $alt_text = "Шановні учасники «ІV Міжнародного форуму аграрних інновацій  «Нове зернятко» - Настасів 2019»!

                    Щиро вдячні за те, що Ваша компанія виявила бажання прийняти участь у Форумі, який відбудеться 20-21 червня 2019 р.
                    Для підтвердження співпраці по участі та оперативної роботи по організації даного заходу, просимо обов’язково роздрукувати два примірники договору (додається у вкладенні), підписані керівника компанії та вислати на поштову адресу:
                    
                    ПАП «Агропродсервіс», с. Настасів, Тернопільський р-н, Тернопільська обл., 47734
                    
                    Після одержання підписаних примірниківв договору на Вашу адресу буде надіслано: ваш примірник договору, специфікацію, рахунок.
                    
                    З повагою оргкомітет Форуму!";


        $mail = new \nz_admin\model\Mail();
        $mail
            ->setRecipients([$partner_info['main_info']['email_v']])
            ->setMailContentSubject("Догорів. Ⅳ-й Міжнародний Форум аграрних інновацій «Нове Зернятко»")
            ->setMailContentBody($html_text)
            ->setMailContentAltBody($alt_text)
            ->setAttachments($dogovyr_patch)
            ->setAttachmentsName("Догорві Нове Зернятко 2019.pdf");

        if ($mail->sendMail())
        {
            $this->db->update(['status' => 4], "partner_main", "id=".$_POST['id']);
            echo json_encode(['true']); exit;
        }else {
            $this->db->update(['status' => 5], "partner_main", "id=".$_POST['id']);
            echo json_encode(['false']); exit;
        }

    }

    /**
     * Провідміняти посаду диреткора
     */
    public function action_name_case_item_posada_dyr()
    {
        if(empty($_POST) OR !isset($_POST['id']) ) $this->redirectToUrl("/nz_admin/partners/");


        $mng_name_case = new \nz_admin\model\NameCase();
        if ($mng_name_case->posadaBossPartner($_POST['id'], true) == true)
        {
            echo json_encode(['true']); exit;
        }else echo json_encode(['Упс, виникла внутрішня помилка, спробуйте пізніше']);


    }

    public function action_create_and_save_spec ()
    {
        if (empty($_POST) or !isset($_POST['id'])) $this->redirectToUrl("/nz_admin/partners/");



        $mng_dog = new \nz_admin\model\CreateDogovyr();

        if ($mng_dog->genSpec($_POST['id']) == true)
        {
            $sert_patch = "files/company_dogovyr/cert/".$_POST['id'].".pdf";
        }else {
            json_encode(['false']); exit;
        }
        $partner_info = (new \nz_admin\model\GetData()) -> getItemAllDataPartner($_POST['id']);

        if ($partner_info['main_info']['status'] != 3 OR !file_exists("files/company_dogovyr/cert/".$_POST['id'].".pdf")) echo json_encode(['Специфікацію не сформовано, сформуйте специфікацію та спрбуйте ще раз!']); exit;

        $html_text = "Специфікація до договору";
        $alt_text = "Специфікація до договору";

        $mail = new \nz_admin\model\Mail();
        $mail
            ->setRecipients([$partner_info['main_info']['email_v']])
            ->setMailContentSubject("Специфікація до договру. Ⅳ-й Міжнародний Форум аграрних інновацій «Нове Зернятко»")
            ->setMailContentBody($html_text)
            ->setMailContentAltBody($alt_text)
            ->setAttachments($sert_patch)
            ->setAttachmentsName("Специфікація до договру Нове Зернятко 2019.pdf");

        if ($mail->sendMail())
        {
            echo json_encode(['true']); exit;
        }else {
            echo json_encode(['false']); exit;
        }

    }
}