<?php

/**
 * Created by PhpStorm.
 * User: bood92
 * Date: 10.02.2019
 * Time: 13:39
 */

class AdminController extends \nz_admin\core\BaseController
{

    protected  $proviryaty_login = false;

    public function action_login () { 

        $this->View
            ->setTitle("LOGIN")
            ->render("home/login", [
                'hashField' => $this->hashField
            ]);
    }

    public function action_validLogin () {

        $this->proviryaty_login = false;

        unset($_SESSION['admin_user']);

        if (empty($_POST)) {
            $this->log->error("Login: Авторизація не виконан, POST дані не передано ip:"._USER_IP);
            $this->redirect404();
        }
        if (!$this->validHashField($_POST['hashField']))
        {
            $this->log->error("Login: Авторизація не виконан, hashField не пройшов валідацю ip:"._USER_IP);
            $this->redirect404();
        }

        $mng_login = new \nz_admin\model\Login();

        $check = $mng_login->LoginUser($_POST['nz_admin_login'], $_POST['nz_admin_password']);
        if ($check)
        {
            $this->log->error("Login: Успішна авторизація, логін:".$_POST['nz_admin_login'].", ip:"._USER_IP);
            $this->redirectToUrl('/nz_admin/partners/');
        }else {
            $this->log->error("Login: Авторизація не виконан, логін:".$_POST['nz_admin_login'].", ip:"._USER_IP);
            $this->redirect404();
        }
        
    }

    

}