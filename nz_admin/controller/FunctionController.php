<?php

/**
 * Created by PhpStorm.
 * User: i.yuzkiv
 * Date: 08.03.2019
 * Time: 9:24
 */
class FunctionController extends  \nz_admin\core\BaseController
{

    public function action_main() {

        $this->View
            ->setTitle("Функції")
            ->setH1Title("Функції")
            ->render("function/main", []);
    }

}