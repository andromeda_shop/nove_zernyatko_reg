<?php

/**
 * Created by PhpStorm.
 * User: i.yuzkiv
 * Date: 11.02.2019
 * Time: 16:44
 */
class VisitorController extends \nz_admin\core\BaseController 
{
    public function action_Visitors () {
        $mng_get_data = new \nz_admin\model\GetData();
        $TableFilter = new \nz_admin\model\TableFilter();

        $TableFilter
            ->setTableData($mng_get_data->getBaseDataVisitors())
            ->setHideRows(["id", "posada_v", "name_factory"])
            ->setHrefTemplate("/nz_admin/visitors/")
            ->setAutoIncrement(true)
            ->setSortField("data_registration")
            ->setRowsCaption([
                "name_factory" => "Назва компанії",
                "pip_v" => "ПІП Уповноваженого",
                "posada_v" => "Посда Уповноваженого",
                "mob_v" => "Моб. Уповноваженого",
                "email_v" => "Email Уповноваженого",
                "status" => "Статус",
                "data_registration" => "Дата реєстрації"
            ]);

        $this->View
            ->setTitle("Відвідувачі форуму")
            ->setH1Title("Відвідувачі форуму")
            ->render("visitors/main",[
                'tableConfig' => $TableFilter->createTable()
            ]);
    }

    public function action_item($id) {

        $data = (new \nz_admin\model\GetData())->getAllDataVisitorsById($id);
        $this->View
            ->setTitle("Відвідувач ".$data['main_info']['name_factory'])
            ->setH1Title("Інфомрація про відвідуванч форуму ".$data['main_info']['name_factory']." ( id = ".$data['main_info']['id'].")")
            ->setUserStyles([
                '/view/_system/css/loading.css'
            ])
            ->render("visitors/item", [
                "data" => $data
            ]);
    }

    public function action_update($id) {
        $mng_update = new \nz_admin\model\UpdateRegData();
        $mng_update->saveBackOriginalData($id, "guests");
        echo $mng_update->updateGuestsData($id, $_POST, "guests");
        exit();
    }

    public function action_create_ticket ($id) {

        $mng_c_ticket = new \nz_admin\model\CreateTicket();
        echo json_encode([$mng_c_ticket->create_ticket($id, 'guests')]); exit();

    }

    public function action_open_lk($id) {
        echo json_encode([(new \nz_admin\model\lk())->open_lk($id, "guests")]); exit;
    }
}