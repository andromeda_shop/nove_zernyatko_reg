<?php

/**
 * Created by PhpStorm.
 * User: bood92
 * Date: 23.02.2019
 * Time: 15:04
 */
class ConfigController extends  \nz_admin\core\BaseController
{
    /**
     * Table configs
     */
    public function action_main () {
        $config_data = $this->db -> setSelect("*") -> setFrom("config") -> all();
        $this->View
            ->setTitle("Config")
            ->render("config/main", [
                'data' => $config_data
            ]);
    }

    /**
     * @param $id
     * Edit json value
     */
    public function action_edit_json_item($id) {

        $config_item = $this->db->setSelect('*')->setFrom('config')->setWhere("id=".$id)->one();
        $config_value = ($config_item['value'] != "") ? json_decode($config_item['value'], true) : null;

        $this->View
            ->setTitle("Edit config id=".$id)
            ->setH1Title("Edit config id = '".$id."', name = '".$config_item['name']."'")
            ->render("config/array_item_edit",[
                'id_config' => $id,
                'data_config' => $config_item,
                'data_value' => $config_value,
                'cnt_value' => count($config_value)
            ]);
    }


    public function action_add_json_item() {
        
        $this->View
            ->setTitle("Add config")
            ->setH1Title("Add config")
            ->render("config/array_item_add",[]);
    }

    /**
     * @param $id
     * Update value
     */
    public function action_update ($id)
    {
        if (empty($_POST)) {
            echo json_encode(array("false"));
            exit();
        }
        $post_data = $_POST;

        if ($id == 0)
        {
            $result = $this->db->insert($post_data, "config", "id=".$id);
        }else {
            $result = $this->db->update($post_data, "config", "id=".$id);
        }

        if ($result)
        {
            echo json_encode(array("true")); exit();
        } else {
            echo json_encode(array("false")); exit();
        }

    }

    /**
     * @param $id
     * Update Json value
     */
    public function action_update_json ($id) {

        if (empty($_POST)){
            echo json_encode(array("false")); exit();
        }

        $post_data = $_POST;
        $post_data['value'] = array();

        $index = 0;
        if (!empty($_POST['value']))
        {
            foreach ($_POST['value'] as $item){
                $post_data['value'][$index] = $item;
                $index++;
            }

            $value = json_encode($post_data['value'], JSON_UNESCAPED_UNICODE);
            $post_data['value'] = $value;
        }

        if ($id == 0)
        {
            $result = $this->db->insert($post_data, "config", "id=".$id);
        }else {
            $result = $this->db->update($post_data, "config", "id=".$id);
        }

        if ($result)
        {
            echo json_encode(array("true")); exit();
        } else {
            echo json_encode(array("false")); exit();
        }

    }

    public function action_delete($id)
    {
        if (empty($_POST)) {
            echo json_encode(array("false"));
            exit();
        }
        if ($this->db->execute("DELETE FROM `config` WHERE id = ".$id))
        {
            echo json_encode(array("true")); exit();
        } else {
            echo json_encode(array("false")); exit();
        }
    }


    /**
     * @param $id
     * Get data item
     */
    public function action_getConfigById ($id) {
        $config_item = $this->db->setSelect('*')->setFrom('config')->setWhere("id=".$id)->one();
        $config_item['modal'] = ($config_item['type_config'] == 'json_params') ? "Json" : "NoJson";
        echo ($config_item !== NULL) ? json_encode($config_item) : json_encode(['false']);
        exit;
    }
    
    
    public function action_log() {
        
    }
    
    
    
}