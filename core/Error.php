<?php
/**
 * Created by PhpStorm.
 * User: i.yuzkiv
 * Date: 10.01.2019
 * Time: 16:05
 */

namespace core;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class Error
{

    /**
     * @param $error_text
     * @param string $log_name
     * @param string $typ_error
     */
    public static function ShowError ($error_text, $log_name = "", $typ_error = "warning", $show_error = true)
    {
        if (_DEBUG == false) {
            header("Location: /404");
            exit();
        }
        if ($show_error) echo $error_text;
        
        if ($log_name !== "") {
            $log = new Logger($log_name);
            $log->pushHandler(new StreamHandler("log/$log_name.log"));
            $log->{$typ_error}($error_text);
        }
    }

    /**
     * @param $error_text
     * @param $log_name
     * @param string $typ_error
     */
    public static function writeLog($error_text, $log_name, $typ_error = "warning") {

        $log = new Logger($log_name);
        $log->pushHandler(new StreamHandler("log/$log_name.log"));
        $log->{$typ_error}($error_text);

    }

}