<?php
/**
 * Created by PhpStorm.
 * User: bood92
 * Date: 25.11.2018
 * Time: 11:11
 */

namespace core;

use core\{
    config,
    Database,
    Registry
};


class BaseModel
{
    /**
     * @var \Smarty
     */
    protected $smart;
    /**
     * @var Database
     */
    protected $db;
    /**
     * @var config
     */
    protected $config;
    /**
     * @var Registry
     */
    protected $Register;


    public function __construct()
    {
        $this->Register = Registry::getInstance();
        $this->smart = $this->Register->get("Smarty");
        $this->db = $this->Register->getByRef("_DB_");
        $this->config = $this->Register->get("_CONFIG_");

    }


    /**
     * @param array $data
     * @return array
     */
    protected function prepare_data_array (Array $data) : array {

        $result = array();
        foreach ($data as $key => $item)
        {
            $result[$key] = trim($item);
        }

        return $result;

    }
}