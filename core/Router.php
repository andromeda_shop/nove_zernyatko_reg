<?php
/**
 * Created by PhpStorm.
 * User: bood92
 * Date: 24.11.2018
 * Time: 17:02
 */


namespace core;
use core\config;
use core\Registry;

class Router
{
    /**
     * @var array
     */
    private $routes;

    /**
     * @var config
     */
    private $config;

    /**
     * Router constructor.
     * Отримуєм масив routs[]
     */
    public function __construct()
    {
        $routesPatch = ROOT."/config/admin_rout.php";
        $this->routes=require_once ($routesPatch);
        $this->config = Registry::get("_CONFIG_");
    }
    /**
     * Получаємо стрічку запиту
     * @return  string
     */
    private function getUri()
    {
        return !empty($_SERVER['REQUEST_URI']) ? trim($_SERVER['REQUEST_URI'],'/') : false;
    }
    /**
     * Запускає потрібний клас та метод коласу, залежно від значення URI (стріча запиту URL)
     */
    public function run()
    {


        $uri = ($this->getUri() == $this->config->get("url_admin_alias")) ? "" : $this->getUri();

        // Перевіряємо чи є такий запит в config/routes.php
        foreach ($this->routes as $uriPattern => $patch) {
            // Порівнюємо вхідний запит із значення в  config/routes.php тепер $patch
            if (preg_match("~$uriPattern~",$uri)) {
                // Получаємо внутрішній шлях (до класу на методу та параметри методу)
                $internalRout = preg_replace("~$uriPattern~",$patch,$uri);
                /*Який контролер controler  та action обробляють запит*/


                $segment = explode('/', $internalRout);
                // опреділяємо назву класа $containerName та назву метода $actionName які потрібно виконати для запиту
                
                $contlollerName = array_shift($segment) . "Controller";
                $contlollerName = ucfirst($contlollerName);


                $actionName = 'action' . ucfirst(array_shift($segment));
                $parametrsAction= $segment;
                // Формується шлях до потрібного файлу з класом та методом
                $controllerFile = ROOT . "/controller/" . $contlollerName . ".php";

                // Якщо файл існує то підключити
                if (file_exists($controllerFile)) {
                    include_once($controllerFile);
                } else {
                    echo "Error: File " . $controllerFile . "no exist.";
                }
                // Створити обєкт та викликати метод
                $controllersObject = new $contlollerName;
                $result = call_user_func_array(array($controllersObject,$actionName),$parametrsAction);
                //$result = $controllersObjec->$actionName($parametrsAction);
                if ($result == NULL)
                {
                    break;
                }
            }
        }
    }
}