<?php

namespace core;

class Registry
{
    /**
     *
     * @var Registry
     */
    static private $_instance = null;

    static private  $_registry = array('empty_value'=>false);

    static public function &getInstance() {
        if (is_null(self::$_instance)) {
            self::$_instance = new self;
        }

        return self::$_instance;
    }

    /**
     * @param string $key
     * @param mixed $object
     */
    static public function set($key, $object) {
        self::$_registry[$key] = $object;
    }
    static public function setByRef($key, &$object) {
        self::$_registry[$key] = &$object;
    }


    /**
     * @param  string $key
     * @return mixed
     */

    static public function get($key) {

        if(isset(self::$_registry[$key])){
            return self::$_registry[$key];
        }else{
            return self::$_registry['empty_value']; //false
        }
    }


    /**
     * @param  string $key
     * @return mixed
     */

    static public function &getByRef($key) {

        if(isset(self::$_registry[$key])){
            return self::$_registry[$key];
        }else{
            return self::$_registry['empty_value']; //false
        }
    }
    /* static public function &getLink($key) {
    	if(isset(self::getInstance()->_registry[$key])){
    		return self::getInstance()->_registry[$key];
    	}else {
    		return false;
    	}
    }*/

    private function __wakeup() {
    }

    private function __construct() {
    }

    private function __clone() {
    }
}

?>