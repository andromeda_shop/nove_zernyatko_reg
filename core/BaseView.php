<?php
/**
 * Created by PhpStorm.
 * User: bood92
 * Date: 25.11.2018
 * Time: 14:03
 */

namespace core;

use core\{
    config,
    Database,
    Registry
};
use model\Menu;

class BaseView
{
    /**
     * @var \Smarty
     */
    protected $smart;
    /**
     * @var Database
     */
    protected $db;
    /**
     * @var config
     */
    protected $config;
    /**
     * @var Registry
     */
    protected $Register;

    /**
     * @var array
     * View system option
     */
    protected $_view_system_option = [
        '_URL'                  =>
            [
                'view'              => _VIEW_,
                'form_field_tpl'    => _VIEW_.'_system/_field_form_group/',
                'system'            => _VIEW_.'_system/',
                'image'            => '/view/_system/image/'
            ],
        'scripts'               => [
            "https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js",
            "/view/_system/js/bootstrap.min.js",
            "/view/_system/js/scripts.js",

        ],
        'styles'                => [
            "/view/_system/css/new_bootstrap.min.css",
            "/view/_system/css/main.css",
            "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css",
            "https://fonts.googleapis.com/css?family=Orbitron",


        ],
        'title'                 => "Title",
        'h1_title'              => "",

    ];

    protected $_base_menu = [
        [
            'title' => 'Реєстрація',
            'alt' => '',
            'alias' => 'registration',
            'href' => '/',
            'active' => 0
        ],
        [
            'title' => 'Особистий кабінет',
            'alt' => '',
            'alias' => 'login',
            'href' => '/login',
            'active' => 0
        ],
        /*[
            'title' => 'Питання відповідь',
            'alt' => '',
            'alias' => 'faq',
            'href' => '/faq',
            'active' => 0
        ]*/
    ];

    public $USER_STYLES = [];
    public $USER_SCRIPTS = [];

    public $current_view;

    /**
     * BaseController constructor.
     */
    public function __construct()
    {
        $this->Register = Registry::getInstance();
        $this->smart = $this->Register->get("Smarty");
        $this->db = $this->Register->get("_DB_");
        $this->config = $this->Register->get("_CONFIG_");

    }

    /**
     * @param string $view
     * @param array $params
     */
    public function render ($view = "", $params = []) {

        $this->smart->assign("_CURRENT_VIEW", $view);

        $view = preg_replace("/.html/", "", strtolower($view));
        $params['view_system_option'] = $this->_view_system_option;
        $params['view_system_option']['include_view'] = _VIEW_.$view.".html";
        $params['view_system_option']['base_menu'] = $this->_base_menu;

        if (!empty($params)) {
            foreach ($params as $name => $value) {
                $this->smart->assign($name, $value);
            }
        }

        $lang_uk = require_once "config/LANG/uk.php";
        $this->smart->assign("LANG_VAR", $lang_uk);

        $this->smart->display(_VIEW_."_system/index.html");
    }

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle ($title = "") {
        $this->_view_system_option['title'] = $title." | Нове Зернятко 2019";
        return $this;
    }

    /**
     * @param string $h1_title
     * @return $this
     */
    public function setH1Title ($h1_title = "") {
        $this->_view_system_option['h1_title'] = $h1_title;
        return $this;
    }

    /**
     * @param string $alias
     * @return $this
     */
    public function setActiveMenu ($alias="") {

        for ($i = 0; $i < count($this->_base_menu); $i++ )
        {
            if ($this->_base_menu[$i]['alias'] == $alias)
            {
                $this->_base_menu[$i]['active'] = 1;
            }else{
                $this->_base_menu[$i]['active'] = 0;
            }
        }

        return $this;
    }

    /**
     * @param array $Styles
     * @return $this
     */
    public function setUserStyles ($Styles = []) {
        if ( !empty($Styles) ) {
            foreach ($Styles as $item) {
                $this->_view_system_option['styles'][] = $item;
            }
        }

        return $this;
    }

    /**
     * @param array $Scripts
     * @return $this
     */
    public function setUserScripts ($Scripts = [])
    {
        if ( !empty($Scripts) ) {
            foreach ($Scripts as $item) {
                $this->_view_system_option['scripts'][] = $item;
            }
        }


        return $this;
    }

}



/*
{$view_system_option|@dump}
-----------------------
Array
(
    [_URL] => Array
        (
            [view] => view/
            [form_field_tpl] => view/_system/_field_form_group/
            [system] => view/_system/
            [image] => /view/_system/image/
        )

    [scripts] => Array
        (
            [0] => https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js
            [1] => /view/_system/js/bootstrap.min.js
            [2] => /view/_system/js/scripts.js
        )

    [styles] => Array
        (
            [0] => /view/_system/css/bootstrap.css
            [1] => /view/_system/css/main.css
            [2] => https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css
            [3] => https://fonts.googleapis.com/css?family=Orbitron
        )

    [title] => Реєстрація на форму, крое 1
    [h1_title] => 
    [include_view] => view/registration/reg_agent_step_one.html
    [base_menu] => Array
        (
            [0] => Array
                (
                    [title] => Реєстрація
                    [alias] => registration
                    [href] => #
                    [active] => 1
                )

            [1] => Array
                (
                    [title] => Особистий кабінет
                    [alias] => lk
                    [href] => #
                    [active] => 0
                )

            [2] => Array
                (
                    [title] => Питання відповідь
                    [alias] => faq
                    [href] => #
                    [active] => 0
                )

        )

)
----------------------- 


 */