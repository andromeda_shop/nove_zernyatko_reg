<?php
namespace core;

use core\database\insert;
use PDO;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class Database
{
    protected       $DBHOST         = 'agrops.mysql.tools';
    protected       $DBNAME         = 'agrops_nz2';
    protected       $DBUSER         = 'agrops_nz2' ;
    protected       $DBPASS         = '5J8@d!o8Ek';
    protected       $rowcount       = 0;
    protected       $link           = 0;
    private         $log;


    protected       $select         = "";
    protected       $from           = "";
    protected       $where          = "";
    protected       $orderBy        = "";
    protected       $limit          = "";

    public $sql = "";


    /**
     * Database constructor.
     */
    public function __construct()
    {
        $this->log = new Logger("db");
        $this->log->pushHandler(new StreamHandler('log/db.log'));
        $this->link = $this->connection();

        return $this;
    }
    /**
     * @return PDO object
     */
    private  function connection ()
    {
        try{
            $this->link = new PDO("mysql:host=". $this->DBHOST .";dbname=".
                $this->DBNAME, $this->DBUSER, $this->DBPASS);
            $this->link->exec("set names utf8");
            return $this->link;
        }
        catch(PDOException $e)
        {
            $this->log->error("Підключення до бази `$this->DBNAME` не вдалось.");
            print("Error: ".$e->getMessage());
            //  exit();
        }
    }
    /**
     * @param $sql
     * @return bool
     *  only execute sql query and return count row;
     */
    public function execute ($sql)
    {
        $sth =  $this->link->prepare($sql);
        $result = $sth->execute();

        return $result;
    }
    /**
     * @param $sql
     * @param $parametr
     * @param $variable
     * @return bool
     */
    public function binParam($sql,$parametr,$variable)
    {
        $sth =  $this->link->prepare($sql);
        $sth->bindParam($parametr,$variable, \PDO::PARAM_STR);
        return $sth->execute();
    }

    /**
     * @return string
     */
    protected function prepareSelectSql () {

        $this->orderBy  = ($this->orderBy != "")                        ? " ORDER BY $this->orderBy "   :   " ";
        $this->where    = ($this->where != "")                          ? " WHERE $this->where "        :   " ";
        $this->select   = ($this->select != "")                         ? $this->select                 :   " * ";
        $this->limit    = ($this->limit != "" OR $this->limit != 0)     ? " LIMIT $this->limit "        :   " ";

        $sql =  "SELECT $this->select FROM $this->from " . $this->where . $this->orderBy . $this->limit ." ; ";

        $this->sql = $sql;

        $this->orderBy  =   "";
        $this->where    =   "";
        $this->select   =   "";
        $this->limit    =   "";

        return $sql;
    }

    /**
     * @param $sql
     * @return array
     * return assoc array
     */
    public function CreateQuery ($sql)
    {
        $sth =  $this->link->prepare($sql);
        $this->sql = $sql;
        $sth->execute();
        $result =  $sth->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    /**
     * @return array
     */
    public function all() {
    $result = $this->CreateQuery($this->prepareSelectSql());
    return (!empty($result)) ? $result : NULL;
}

    /**
     * @return array
     */
    public function one() {
        $this->setLimit(1);
        $result = $this->CreateQuery($this->prepareSelectSql());
        return (!empty($result)) ? $result[0] : NULL;
    }

    /**
     * @return Query
     */
    public function Query() {
        return  (new \core\database\query());
    }

    public function insert ($field = [], $table_name) {

        if (!empty($field))
        {

            $column = implode(" , ", array_keys($field));

            foreach ($field as $key => $item)
            {
                $field[$key] = str_replace('"',"&#34;", $item);
                $field[$key] = str_replace("'","&#34;", $item);
            }

            $values = implode("' , '", $field);
            $sql =  " INSERT INTO $table_name ( " .$column. " ) VALUES (  '".$values."' );";
            $this->sql = $sql;
            return $this->execute($sql);

        }else
            return false;

    }

    public function insert_get_id ($field = [], $table_name) {

        if (!empty($field))
        {

            $column = implode(" , ", array_keys($field));

            foreach ($field as $key => $item)
            {
                $field[$key] = str_replace('"',"&#34;", $item);
                $field[$key] = str_replace("'","&#34;", $item);
            }

            $values = implode("' , '", $field);
            $sql =  " INSERT INTO $table_name ( " .$column. " ) VALUES (  '".$values."' );";
            $this->sql = $sql;

            try {
                $sth =  $this->link->prepare($sql);
                $result = $sth->execute();
                $id = $this->link->lastInsertId();
                return $id;
            } catch (\PDOException $e) {
                return "Error!: " . $e->getMessage() . "</br>";
            }

        }else
            return false;

    }

    public function update ($field = [], $table_name, $where = "") {

        if (!empty($field))
        {

            //$column = implode(" , ", array_keys($field));
            //$values = implode("' , '", $field);

            $update_set = "";
            $i = 1;
            foreach ($field as $key => $item) {
                $i++;
                $update_set .= "`" . $key . "`='$item'";
                $update_set .= ($i <= count($field)) ? " , " : " ";
            }

            $update_set .= ($where != "") ? " WHERE $where" : "WHERE 1 ";
            $sql = "UPDATE `$table_name` SET $update_set;";

            //echo $sql;

            $this->sql = $sql;
            return $this->execute($sql);

        }else
            return false;

    }


    /**
     * @param string $select
     * @return $this
     */
    public function setSelect($select = "") {
        if (is_string($select)) {
            $this->select = $select;
        }else {
            $this->select = implode(" , ", $select);
        }

        return $this;
    }
    /**
     * @param $from
     * @return $this
     */
    public  function  setFrom($from = "")
    {
        $this->from = $from;
        return $this;
    }

    /**
     * @param string $where
     * @return $this
     */
    public function setWhere($where = "") {
        $this->where = $where;
        return $this;
    }

    /**
     * @param $limit
     * @return $this
     */
    public function setLimit ($limit) {
        $this->limit = $limit;
        return $this;
    }

    /**
     * @param string $orderBy
     * @return $this
     */
    public function setOrderBy ($orderBy = "") {
        $this->orderBy = $orderBy;
        return $this;
    }

}