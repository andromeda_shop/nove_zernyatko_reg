<?php
/**
 * Created by PhpStorm.
 * User: bood92
 * Date: 15.10.2018
 * Time: 21:05
 */

namespace core;

use core\Registry;
use Mpdf\Tag\P;

class config
{

    private $db;

    public function __construct()
    {
        $Register = Registry::getInstance();
        $this->db = $Register->get("_DB_");
    }

    /**
     * @param $name
     * @return array
     */
    public function get($name) {
        //return (new Database()) -> query("SELECT `value` FROM `config` WHERE `name` LIKE '$name' LIMIT 1;")[0]['value'];

        $result = $this->db->Query()
            ->setFrom('config')
            ->setWhere("name = '".$name."'")
            ->one();

        if ($result['type_config'] == "json_params" and $result['value'] != "") {
            $result['value'] = json_decode($result['value'], true);
        }

        if (isset($result['value']))
            return $result['value'];
        else
            return NULL;

    }
    /**
     * @param string $name
     * @param string $value
     * @return bool
     */
    public function set($name = "", $value = "") {
        if ($value != "" && $name != "") {
            (new Database()) -> execute("UPDATE `config` SET `value` = '$value' WHERE `name` LIKE '$name' ;");
        }else return false;
    }


    public function prepareJsonConfig ($array)
    {
        $result = array();

        foreach ($array as $item) {
            $result[$item['name']] = $item['val'];
        }

        return $result;
    }



}