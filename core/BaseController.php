<?php
/**
 * Created by PhpStorm.
 * User: bood92
 * Date: 24.11.2018
 * Time: 17:12
 */

namespace core;


use core\BaseView;
use core\config;
use core\Database;
use core\Registry;
use model\AdminUsers;

class BaseController
{
    /**
     * @var \Smarty
     */
    protected $smart;
    /**
     * @var Database
     */
    protected $db;
    /**
     * @var config
     */
    protected $config;
    /**
     * @var Registry
     */
    protected $Register;

    protected $hashField = "";
    protected $hashFieldLen;
    private $file_hash_data = "asdasdas2dd_hash_field";

    /**
     * @var BaseView
     */
    protected $View;
    /**
     * BaseController constructor.
     */
    public function __construct() {
        $this->Register = Registry::getInstance();
        $this->smart = $this->Register->get("Smarty");
        $this->db = $this->Register->get("_DB_");
        $this->config = $this->Register->get("_CONFIG_");
        $this->View = new \core\BaseView();
        $this->getHashField();
    }

    protected function getHashField() {
        $hash_data = explode(",",file_get_contents($this->file_hash_data));
        $this->hashFieldLen = $hash_data[0];
        $this->hashField = $hash_data[1];
    }
    protected  function validHashField($_post_data="") {
        if ($_post_data != "" or $_post_data != NULL)
        {
            if (mb_strlen($_post_data) !=  $this->hashFieldLen)
            {
                return false;
            }else return true;

        }else return false;
    }


    /**
     *
     */
    protected function redirect404 () {
        echo '<script type="application/javascript"> location.href="/404" </script>';
    }

    /**
     * @param $url
     */
    protected function redirectToUrl($url) {
        if ($url != NULL)
        {
            echo '<script type="application/javascript"> location.href="'.$url.'" </script>';
        }
    }




    

}