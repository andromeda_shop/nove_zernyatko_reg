<?php

/**
 * Created by PhpStorm.
 * User: i.yuzkiv
 * Date: 24.10.2018
 * Time: 12:25
 */

namespace core\database;


use core\Database;

class query extends Database
{
    /**
     * @return array|bool
     */
    public function all() {
        $sql = $this->prepareSelectSql();
        try {
            return $this->CreateQuery($sql);
        }catch (\Exception $e) {
            print_r("Error: ". $e->getMessage());
            return false;
        }
    }

    /**
     * @return array|bool
     */
    public  function one () {

        $this->limit = 1;
        $sql = $this->prepareSelectSql();
        try {
            $result = $this->CreateQuery($sql);
            if (isset($result[0]))
                return $result[0];
            else
                return array();

        }catch (\Exception $e) {
            print_r("Error: ". $e->getMessage());
            return false;
        }

    }



}