/**
 * Created by bood92 on 26.01.2019.
 */

var $ = jQuery;


$(this).keydown(function(eventObject){
    if (eventObject.which == 27)
        $(".select_option_div").hide();
});


function showMsgError (textError, headerText) {
    $("#errorBoxModal").find(".modal-body").html(textError);

    if (headerText != "" || headerText != undefined || headerText != null)
    {
        $("#errorBoxModal").find(".modal-header span.headerError").html(headerText);
    }

    $("#errorBoxModal").modal();
}

$(document).on('change', ':file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
});

$(document).ready(function(){
    ON_PAGE_LOAD_1();
});


